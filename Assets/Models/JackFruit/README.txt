Readme

Animation Cycle (24 FPS)

1) Jackfruit Walk           | Frame 01 - 24
2) Jackfruit Attack         | Frame 25 - 48
3) Jackfruit Damaged        | Frame 49 - 72
4) Jackfruit Idle           | Frame 73 - 120 
5) Jackfruit Death          | Frame 121 - 168
