﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.AI;
public class FingerMove : MonoBehaviour,IPointerClickHandler{
    bool fairy_attack;
    private GameObject player,fairy,cam,canvas;
    public Transform test;
    NavMeshAgent nav;
    Animator anim;
    void Start(){

        player = GameObject.FindGameObjectWithTag("TurnManager")
		.GetComponent<TurnManager>()
		.playerList[0];
        fairy = GameObject.FindGameObjectWithTag("Fairy");
        cam = GameObject.FindGameObjectWithTag("MainCamera");
        anim = player.transform.GetChild(0).GetComponent<Animator>();
        nav = player.transform.GetComponent<NavMeshAgent>();

        /*
        touch = new TouchGesture(this.GestureSetting);
        StartCoroutine(touch.CheckHorizontalSwipes(
            onLeftSwipe: () => { 
                //Debug.Log("swipe left");
                canvas = GameObject.FindGameObjectWithTag("MainCanvas");
                if(!canvas.GetComponent<CanvasScript>().window_open)
                    changeFairyMode(false);
             },
            onRightSwipe: () => { 
                //Debug.Log("swipe right");
                canvas = GameObject.FindGameObjectWithTag("MainCanvas");
                if(!canvas.GetComponent<CanvasScript>().window_open)
                    changeFairyMode(true);
             }
        ));
        */
    }
    void Update(){
        
    }
    IEnumerator TakeDamage(GameObject enemy2,GameObject gameObject,int damage) {
        yield return new WaitForSeconds(0.5f);
        enemy2.GetComponent<Enemy>().TakeDamage(gameObject,damage); 
    }
    public void stayStill(){
        nav.Stop();
        anim.SetFloat ("Move",Mathf.Abs(0));
    }
    public void moveTowardPosition(Transform position){
        nav.Resume();
        nav.SetDestination(position.position);
        anim.SetFloat ("Move",Mathf.Abs(5));
        //camera.GetComponent<PlayerCamera>().ROTATE = false;
    }
	public void OnPointerClick (PointerEventData data){
        
        
    }
    private void fairyAttack(GameObject enemy){
        
        if(fairy!= null){
            GameObject attack = Instantiate(Resources.Load<GameObject>("Prefabs/FairySword")) as GameObject;
            attack.transform.SetParent(fairy.transform, false);
            attack.GetComponent<FairyLaser>().shootLaser(fairy,enemy);
        }
    }
    void fairyExecute(){
        
        if(fairy_attack){
            PlayerAttack player_attack = player.transform.GetChild(0).GetComponent<PlayerAttack>();
            GameObject enemy = player_attack.findEnemy(player);
            GameObject enemy_boss = player_attack.findEnemyBoss(player);
            if(enemy != null){
                //fairy attack
                fairyAttack(enemy);
                int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
                enemy.GetComponent<Enemy>().TakeDamageFairy(enemy,damage);
                
            }else if(enemy_boss != null)
            {
                 fairyAttack(enemy_boss);
                 int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
                 enemy_boss.GetComponent<Enemy>().TakeDamageFairyBoss(enemy_boss,damage);
            }
            
            
        }else{
            PlayerHealth player_health = player.GetComponent<PlayerHealth>();
            int heal = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.HP).baseValue;
            player_health.Heal(heal);
            
        }
        
    }
    public void fairySkill(int num,BaseSkill skill){
       switch (num)
       {
           //fairy skill 1
           case 0:
            GameObject attack = Instantiate(Resources.Load<GameObject>("Particle/Purple Big Bang")) as GameObject;
            attack.transform.SetParent(GameObject.FindGameObjectWithTag("SpawnManager").transform.GetChild(0), false);
            Destroy(attack,1);
           break;
           //fairy skill 2
           case 1:
            GameObject heal = Instantiate(Resources.Load<GameObject>("Particle/FairyHeal_skill")) as GameObject;
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            heal.transform.SetParent(player.transform, false);
            int time_skill = SkillList.getSkillPlayer(player.GetComponent<DoranPlayer>(),SkillList.FAIRY_SKILL_2).SkillDuration;
            Debug.Log("skill heal duration: "+time_skill);
            StartCoroutine (DestroyParticle(heal,time_skill));
           break;
       }
       
    }
    public void doranSkill(int num,BaseSkill skill){
         switch (num)
       {
           //doran skill 1
           case 0:
            PlayerAttack player_attack = player.transform.GetChild(0).GetComponent<PlayerAttack>();
            GameObject enemy = player_attack.findEnemy(player);
            GameObject enemy_boss = player_attack.findEnemyBoss(player);
            if(enemy != null){
                GameObject attack = Instantiate(Resources.Load<GameObject>("Particle/Wind")) as GameObject;
                attack.transform.SetParent(player.transform, false);
                attack.AddComponent<FairyLaser>().shootHurricane(player,enemy);
                Destroy(attack,1);
                //temporary damage
                int damage = player.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).baseValue + 
                player.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).modifiedValue;
                enemy.GetComponent<Enemy>().TakeDamage(player,200+damage);
            }else if(enemy_boss != null)
            {
                GameObject attack = Instantiate(Resources.Load<GameObject>("Particle/Wind")) as GameObject;
                attack.transform.SetParent(player.transform, false);
                attack.AddComponent<FairyLaser>().shootHurricane(player,enemy_boss);
                Destroy(attack,1);
                //temporary damage
                int damage = player.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).baseValue + 
                player.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).modifiedValue;
                enemy_boss.GetComponent<Enemy>().TakeDamageBoss(player,200+damage);
            }
           break;
           //doran skill 2
           case 1:
           break;
       }
    }
    private IEnumerator DestroyParticle(GameObject skill,int time_skill2){
        while(time_skill2>0){
            PlayerHealth player_health = player.GetComponent<PlayerHealth>();
            int heal = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.HP).baseValue;
            player_health.Heal(heal);
            time_skill2--;
            yield return new WaitForSeconds(1);
        }
        Destroy(skill);
    }
    public void changeFairyMode(bool right){
        fairy_attack = !fairy_attack;
        /*
        GameObject mode = GameObject.FindGameObjectWithTag("MainCanvas").transform.GetChild(CanvasScript.FAIRY_MODE).gameObject;
        if(right)
            mode.GetComponent<Animator>().SetTrigger("right");
        else
            mode.GetComponent<Animator>().SetTrigger("left");

        GameObject mode2 = GameObject.FindGameObjectWithTag("UpgradePanel").transform.GetChild(3).gameObject;
        GameObject fairy = GameObject.FindGameObjectWithTag("Fairy");
        GameObject particle = fairy.transform.GetChild(1).gameObject;
        if(particle!= null){
            Destroy(particle);
        }
        if(fairy_attack){
            //mode.GetComponent<Image>().color= Color.red;
            //mode.transform.GetChild(0).GetComponent<Text>().text = "Fairy Mode: ATTACK";
            mode.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/fairy_toggle_attack");
            //should start attack toggle

            mode2.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/fairy");
            fairy.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.red;
            GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Fireflies_ATK")) as GameObject;
            skillparticle.transform.SetParent(fairy.transform, false);
        }else{
            //mode.GetComponent<Image>().color= Color.green;
            //mode.transform.GetChild(0).GetComponent<Text>().text = "Fairy Mode: HEAL";
            mode.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/fairy_toggle_heal");
            //should start heal toggle

            mode2.GetComponent<Image>().sprite = Resources.Load<Sprite>("UI/fairy_heal");
            fairy.transform.GetChild(0).GetComponent<Renderer>().material.color = Color.green;
            GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Fireflies_HEAL")) as GameObject;
            skillparticle.transform.SetParent(fairy.transform, false);
        }
        */
        GameObject mode2 = GameObject.FindGameObjectWithTag("UpgradePanel").transform.GetChild(3).gameObject;
        mode2.GetComponent<Animator>().SetTrigger("changemode");
    }
    
}
