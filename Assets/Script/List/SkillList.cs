﻿using UnityEngine;
using System.Collections;

public class SkillList : MonoBehaviour {

	public static int FAIRY_SKILL_1 = 0;
	public static int FAIRY_SKILL_2 = 1;
	public static int FAIRY_PASSIVE_1 = 2;
	public static int FAIRY_PASSIVE_2 = 3;
	public static int DORAN_SKILL_1 = 4;
	public static int DORAN_SKILL_2 = 5;
	public static int DORAN_SKILL_3 = 6;
	
	public static BaseSkill getSkillInfo(int id){
		BaseSkill skill = new BaseSkill();
		switch (id)
		{
			case 0:
			
			skill.Name = "THE DARK HARVEST";
			skill.skillCooldown = 60;
			skill.skillDuration = 1;
			skill.skillDescription = "The fairy drops bountiful sorrow";
			skill.damageMultiplier = 100;
			skill.image = "Fairy_skill_magic";
			return skill;
			
			case 1:
			skill.Name = "MEGA HEAL";
			skill.skillCooldown = 100;
			skill.skillDuration = 1;
			skill.skillDescription = "The goddess decend to bless the adventurer";
			skill.damageMultiplier = 100;
			skill.image = "Fairy_skill_megaheal";
			return skill;
			
			case 2:
			skill.Name = "DURIAN RUNTUH";
			skill.skillDescriptSmall = "+5% DE";
			skill.Description = "Adds Durian drop percentage!";
			skill.damageMultiplier = 5;
			skill.image = "Fairy_passive_durianruntuh";
			return skill;
			
			case 3:
			skill.Name = "LADY LUCK";
			skill.skillDescriptSmall = "+3% LUCK";
			skill.Description = "Increases percentage of chest encounter";
			skill.damageMultiplier = 3;
			skill.image = "Fairy_passive_luck";
			return skill;
			
			case 4:
			skill.Name = "HURRICANE SLASH";
			skill.skillCooldown = 100;
			skill.skillDuration = 1;
			skill.skillDescription = "Slash and sing like a hurricane";
			skill.damageMultiplier = 200;
			skill.image = "Doran_hurricane";
			return skill;
			
			case 5:
			skill.Name = "DOPPELGANGER";
			skill.skillCooldown = 100;
			skill.skillDuration = 1;
			skill.skillDescription = "Summon your shadow to fight for you for a moment";
			skill.damageMultiplier = 200;
			skill.image = "Doran_dopplerganger";
			return skill;
			
			case 6:
			skill.Name = "DURI MODE";
			skill.skillCooldown = 100;
			skill.skillDuration = 1;
			skill.skillDescription = "Legend foretold that a spike monster will befall for those who ate too much.";
			skill.damageMultiplier = 200;
			skill.image = "Doran_eDurian";
			return skill;
			
			default:
			break;
		}
		
		return null;
	}
	public static BaseSkill getSkillPlayer(DoranPlayer player,int id){
		return player.player.baseSkill.Find(w => string.Equals(w.Name, getSkillInfo(id).Name));
	}
	public static BaseSkill getPanelPlayer(DoranPlayer player,int id){
		return player.player.basePanel.Find(w => string.Equals(w.Name, getSkillInfo(id).Name));
	}
}
