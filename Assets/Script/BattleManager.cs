﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.AI;

public class BattleManager : MonoBehaviour {
	GameObject battleMode,maincanvas;
	public List<string> turns_list,bucket_list;
	DoranState playerState;
	public GameObject tutorialPanel;
	

	// Use this for initialization
	void Start () {
		maincanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        battleMode = maincanvas.transform.GetChild(CanvasScript.BATTLE_MODE).gameObject;
		playerState = GameObject.FindGameObjectWithTag("TurnManager")
		.GetComponent<TurnManager>()
		.playerList[0]
		.GetComponent<DoranState>();
		battleMode = maincanvas.transform.GetChild(CanvasScript.BATTLE_MODE).gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void showPlayerSelection(){
		if(!battleMode.transform.GetChild(0).gameObject.activeSelf)
			battleMode.transform.GetChild(0).gameObject.SetActive(true); 
	}
	public void closePlayerSelection(){
		if(battleMode.transform.GetChild(0).gameObject.activeSelf)
			battleMode.transform.GetChild(0).gameObject.SetActive(false); 
	}
	public void showBattleMenu(){
		if(!battleMode.activeSelf)
			battleMode.SetActive(true);
	}
	public void closeBattleMenu(){
		if(battleMode.activeSelf)
			battleMode.SetActive(false);
	}
	public void showSkillMenu(){
		if(!maincanvas.transform.GetChild(CanvasScript.SKILL).gameObject.activeSelf)
			maincanvas.transform.GetChild(CanvasScript.SKILL).gameObject.SetActive(true);
	}
	public void closeSkillMenu(){
		if(maincanvas.transform.GetChild(CanvasScript.SKILL).gameObject.activeSelf)
			maincanvas.transform.GetChild(CanvasScript.SKILL).gameObject.SetActive(false);
	}
	public void showTutorial(){
		showTutorialPanel(true);
        maincanvas.GetComponent<CanvasScript>().showPanelText("Choose Your Actions!");
	}
    public void closeTutorial(){
		showTutorialPanel(false);
	}
	void checkEnemyPhase(){
		//PlayerAttack player_attack3 = transform.GetChild(0).GetComponent<PlayerAttack>();
        //GameObject enemy = player_attack3.findEnemy(gameObject);
	}
	public void monsterTurn(GameObject enemy){
        enemy.transform.GetChild(2).GetComponent<EnemyAttack>().Attack();
    }
    public IEnumerator endTurn(){
        
        turns_list = turns_list.Skip(1).Concat(turns_list.Take(1)).ToList();

        updateTurnUI();

        battleMode.transform.GetChild(0).gameObject.SetActive(false);

        yield return new WaitForSeconds(1);
        //change state depends on whose turn
        if(turns_list[0] == "doran")
            playerState.changeStateAndExecuteTurn(DoranState.State.CHOOSE);
        else
            playerState.changeStateAndExecuteTurn(DoranState.State.WAIT);
    }
    void updateTurnUI(){
        for(int i=0;i<5;i++){
            GameObject button = battleMode.transform.GetChild(2).GetChild(i).gameObject;
            switch(turns_list[i]){
                case "doran":
                button.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("Doran_dp");
                break;

                case "toge":
                button.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("");
                break;
            }
            button.transform.GetChild(0).GetComponent<Image>().enabled = true;
        }
    }
	public void addTurnBasedOnSpeed(){
        //temp turn
        turns_list = new List<string>();
        turns_list.Add("doran");
        turns_list.Add("toge");
        turns_list.Add("doran");
        turns_list.Add("toge");
        turns_list.Add("doran");
        turns_list.Add("toge");
        updateTurnUI();

        bucket_list = new List<string>();
        //clear bucket
        Transform bucket = battleMode.transform.GetChild(3).GetChild(1);
        for(int i=0;i<5;i++){
            if(bucket.GetChild(i).childCount > 0){
                Destroy(bucket.GetChild(i).GetChild(0).gameObject);
            }
        }
    }
	public void addDurianToBucket(){
        //Debug.Log("Added durian to bucket "+bucket_list.Count);
        if(bucket_list.Count < 5){
            //random durian
            int random = UnityEngine.Random.Range(0,3);
            if(random == 0)
                bucket_list.Add("blue");
            else if(random == 1)
                bucket_list.Add("red");
            else
                bucket_list.Add("yellow");
           
            //add durian + animation
            GameObject obj = Instantiate(Resources.Load<GameObject>("Prefabs/DurianColor")) as GameObject;
            obj.transform.SetParent(battleMode.transform.GetChild(3).GetChild(1).GetChild(bucket_list.Count-1),false);
            obj.GetComponent<Animator>().SetTrigger("getdurian");
            switch(bucket_list[bucket_list.Count-1]){
                case "red":
                obj.GetComponent<Image>().sprite = ImageManager.getSpriteFromSpriteSheet("UI/ui_new","ui_new_24");
                break;

                case "blue":
                obj.GetComponent<Image>().sprite  = ImageManager.getSpriteFromSpriteSheet("UI/ui_new","ui_new_25");
                break;

                case "yellow":
                obj.GetComponent<Image>().sprite  = ImageManager.getSpriteFromSpriteSheet("UI/ui_new","ui_new_26");
                break;

            }
            //updateBucket();
        }
    }
    public void removeDurianFromBucketNum(int num){
        bucket_list.RemoveAt(num);
    }
    public void removeDurianFromBucket(int num){
        if(bucket_list.Count > 0){
            
            bucket_list.RemoveRange(0,num);
            //remove durian + animation
            for(int i=0;i<num;i++){
                GameObject durian = battleMode.transform.GetChild(3).GetChild(1).GetChild(i).GetChild(0).gameObject;
                durian.GetComponent<Animator>().SetTrigger("removedurian");
                Destroy(durian,1);
            }
            //shift durian counterclockwise

            if(bucket_list.Count > 0){
                StartCoroutine(ShiftLeft(num));
            }
        }else{
            //error
        }
        //updateBucket();
    }
    
    IEnumerator ShiftLeft(int num) {
        //Debug.Log("shift "+num);
        float delay = 1;
            
        yield return new WaitForSeconds(delay);
        Transform bucket = battleMode.transform.GetChild(3).GetChild(1);
        for(int i=1;i<5;i++){
            if(bucket.GetChild(i).childCount > 0){
                Transform durian = bucket.GetChild(i).GetChild(0);
                durian.SetParent(bucket.GetChild(i-num));
                durian.position = bucket.GetChild(i-num).position;
            }
        }
    }
    public int checkbucketList(){
        return bucket_list.Count;
    }
    public List<string> getBucket(){
        return bucket_list;
    }
	void showTutorialPanel(bool show){
        tutorialPanel.SetActive(show);
    }
}
