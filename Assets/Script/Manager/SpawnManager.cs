﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class SpawnManager : MonoBehaviour {

    public int CURRENT_DEPTH = 1;
    public int CURRENT_MODULE = 0;
    public int CURRENT_PHASE = 0;
    int random = 0;
    public List<DoranState.State> moduleList;
    public GameObject[] objectList;
    public Transform[] spawnPoint;
    public Text txt_depth;
    public GameObject[] levelList;
    public int module_length;
    GameObject tutorialManager,depthManager;
    public static bool TUTORIAL = false;
    public Transform currentSpawnPoint;
    GameObject cam;
	
	void Start () {
        
	    tutorialManager = GameObject.FindGameObjectWithTag("TutorialManager");
        depthManager = GameObject.FindGameObjectWithTag("Depth");
        cam = GameObject.FindGameObjectWithTag("ChangeCamera");

        // if(TUTORIAL)
        //     startTutorial();
        // else
        initializeRoute();
            
        //length of travel before go down
        //example: 4 for depth 1-3 , 5 for depth 4-6, 6 for upper depths. 3 for special depths
        // randomize the inside condition , but boss and godown remain
        //example: monster -> monster -> item  -> boss(godown)
	}
	public void initializeRoute(){
        CURRENT_MODULE = 0;
        CURRENT_PHASE = 0;
        
        module_length = 3;
        txt_depth.text = ""+CURRENT_DEPTH;
        
        //make a list
        //arrange based on rarity
        // battle 80% , item 20%
        
        //example list
        
        moduleList = new List<DoranState.State>();
        //moduleList.Add(DoranState.State.NOTUTORIAL);
        moduleList.Add(DoranState.State.BATTLE);
        //moduleList.Add(DoranState.State.GODOWN);
        
        //updateLevelList();
        
    }
    public void startTutorial(){
        
        CURRENT_MODULE = 0;
        CURRENT_PHASE = 0;
        
        module_length = 3;
        txt_depth.text = ""+CURRENT_DEPTH;
         //tutorial
        moduleList = new List<DoranState.State>();
        tutorialManager.GetComponent<TutorialManagerScript>().executeTutorial();
        moduleList.Add(DoranState.State.START);
        moduleList.Add(DoranState.State.TUTORIAL);
        moduleList.Add(DoranState.State.START);
        moduleList.Add(DoranState.State.TUTORIAL);
        moduleList.Add(DoranState.State.ITEM);
        moduleList.Add(DoranState.State.START);
        moduleList.Add(DoranState.State.BOSS);
        moduleList.Add(DoranState.State.TUTORIAL); //get egg
        moduleList.Add(DoranState.State.GODOWN);
        
        
    }
    //not using this atm
    void updateLevelList(){
        for(int i=0;i<depthManager.transform.GetChild(3).childCount;i++){
            Destroy(depthManager.transform.GetChild(3).GetChild(i).gameObject);
        }
        for(int i=0;i<moduleList.Count;i++){
            if(moduleList[i] == DoranState.State.START){
                GameObject obj = Instantiate(Resources.Load("UI/Level")) as GameObject;
                obj.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("default");
                obj.transform.SetParent(depthManager.transform.GetChild(3));
            }
            if(moduleList[i] == DoranState.State.NOTUTORIAL){
                GameObject obj = Instantiate(Resources.Load("UI/Level")) as GameObject;
                obj.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("default");
                obj.transform.SetParent(depthManager.transform.GetChild(3));
            }
            else if(moduleList[i] == DoranState.State.BATTLE){
                GameObject obj = Instantiate(Resources.Load("UI/Level")) as GameObject;
                obj.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("mon_toge");
                obj.transform.SetParent(depthManager.transform.GetChild(3));
            }
            else if(moduleList[i] == DoranState.State.ITEM){
                GameObject obj = Instantiate(Resources.Load("UI/Level")) as GameObject;
                obj.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("Doran_defend");
                obj.transform.SetParent(depthManager.transform.GetChild(3));
            }
            else if(moduleList[i] == DoranState.State.BOSS){
                GameObject obj = Instantiate(Resources.Load("UI/Level")) as GameObject;
                obj.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("death_menu");
                obj.transform.SetParent(depthManager.transform.GetChild(3));
            }
        }
    }
    public void nextRandomModule(){
        //x random lagi
        CURRENT_MODULE = 0;
        CURRENT_PHASE = 0;
        
        CURRENT_DEPTH++;
        if(CURRENT_DEPTH > 2)
            module_length++;
            
        //example list
        moduleList = new List<DoranState.State>();
        for(int i=0;i<module_length;i++){
           moduleList.Add(DoranState.State.BATTLE);
        }
        moduleList.Add(DoranState.State.ITEM);
        moduleList.Add(DoranState.State.BOSS);
        moduleList.Add(DoranState.State.GODOWN);
        
        //GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<Animator>().SetTrigger("stage_change");
        //spawnNewStage();
        //updateLevelList();
        
    }
    public void spawnNewStage(){
        GameObject stage = GameObject.FindGameObjectWithTag("Stage");
        if(stage != null)
            Destroy(stage);
            
        //int i = Random.Range(0,levelList.Length);
        int i = 0;
        //if(CURRENT_DEPTH > 1)
        //    i=1;
            
        //GameObject level = Instantiate(levelList[i],transform.position,Quaternion.identity) as GameObject;
        //level.transform.SetParent(transform);
        
        //spawnModule();
    }
    public void spawnModule(){
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        GameObject obj = null;
        txt_depth.text = "Stage"+CURRENT_DEPTH+" - "+CURRENT_MODULE;

        
        if(random > spawnPoint.Length-1)
            random=0;
        //int random = Random.Range(0,spawnPoint.Length);
        currentSpawnPoint = spawnPoint[random];
        
        
        if(random > 0){
            cam.SetActive(true);
            cam.GetComponent<ChangeCamera>().changeColliderPos(random);
        }else{
            cam.SetActive(false);
        }
        switch (moduleList[CURRENT_MODULE])
        {
            case DoranState.State.START:
            player.transform.GetChild(0).gameObject.SetActive(true);
            obj = Instantiate(objectList[3]) as GameObject;
            obj.transform.SetParent(spawnPoint[random].transform,false);

            //startLevelList();
            break;
            
            case DoranState.State.BATTLE:
            //depthManager.GetComponent<Animator>().SetTrigger("depth_start");
            
            player.transform.GetChild(0).gameObject.SetActive(true);
            obj = Instantiate(objectList[0]) as GameObject;
            obj.transform.SetParent(spawnPoint[random].transform,false);
            
            startLevelList();
            break;
            
            case DoranState.State.ITEM:
            //depthManager.GetComponent<Animator>().SetTrigger("depth_start");
            
            obj = Instantiate(objectList[1]) as GameObject;
            obj.transform.SetParent(spawnPoint[random].transform,false);
            
            if(!TUTORIAL)
                startLevelList();
            break;
            
            case DoranState.State.BOSS:
            //depthManager.GetComponent<Animator>().SetTrigger("depth_start");
            
            obj = Instantiate(objectList[2]) as GameObject;
            obj.transform.SetParent(spawnPoint[random].transform,false);
            
            if(!TUTORIAL)
                startLevelList();
            break;
            
            case DoranState.State.GODOWN:
            //bug 2 monster spawn
            Debug.Log("spawning new level");
            
            //transform.GetChild(4).gameObject.SetActive(false);
            nextRandomModule();
            break;
            
            case DoranState.State.TUTORIAL:
            tutorialManager.GetComponent<TutorialManagerScript>().executeTutorial();
            break;
            
            case DoranState.State.NOTUTORIAL:
            tutorialManager.GetComponent<TutorialManagerScript>().noTutorial();
            player.transform.GetChild(0).gameObject.SetActive(true);
            obj = Instantiate(objectList[0]) as GameObject;
            obj.transform.SetParent(spawnPoint[random].transform,false);
            
            startLevelList();
            break;
            
            default:
            Debug.Log("Error, module not found?");
            break;
        }
        
        //player.GetComponent<DoranState>().currentState = moduleList[CURRENT_MODULE];
        player.GetComponent<DoranState>().changeStateAndExecuteTurn(moduleList[CURRENT_MODULE]);

        if(moduleList[CURRENT_MODULE] != DoranState.State.GODOWN)
            CURRENT_MODULE++;

        random++;
    }
    void startLevelList(){
        //depthManager.SetActive(true);
        //depthManager.GetComponent<Animator>().SetTrigger("depth_start");
    }
    public void triggerLevel(){
        depthManager.transform.GetChild(3).GetChild(CURRENT_PHASE).GetComponent<Animator>().SetTrigger("selected");
        if(CURRENT_PHASE != 0){
            depthManager.transform.GetChild(3).GetChild(CURRENT_PHASE-1).GetComponent<Animator>().SetTrigger("diselect");
        }
        CURRENT_PHASE++;
    }
    public void setColorGrey(){
        if(CURRENT_PHASE != 0){
            //Debug.Log("changing color to grey");
            depthManager.transform.GetChild(3).GetChild(CURRENT_PHASE-1).GetChild(0).GetComponent<Image>().color = Color.grey;
        }
    }
}
