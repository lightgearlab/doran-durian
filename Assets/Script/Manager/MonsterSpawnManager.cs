﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class MonsterSpawnManager : MonoBehaviour {

	public GameObject[] monsterPrefabs;
	public float spawnRadius;
	public int totalEnemy;
	
	void Start () {
		Spawn();
		GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<ChangeScene>().changeSceneBattle();
		// GameObject currentPlayer = GameObject.FindGameObjectWithTag("TurnManager")
		// .GetComponent<TurnManager>()
		// .playerList[0];
		// currentPlayer.GetComponent<DoranState>().changeStateAndExecuteTurn(DoranState.State.BATTLE);
		//SpawnMonster();
	}
	void SpawnMonster(){
		Debug.Log("Spawning "+monsterPrefabs[0]);
		GameObject monster = Instantiate(monsterPrefabs[0]) as GameObject;
		monster.transform.SetParent(transform.parent.transform,false);

		EnemyManager enemyManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<EnemyManager>();
        //init health
        enemyManager.InitializeHP();
        float maxhealth = 0,health = 0;
        List<float> enemyHealth = new List<float>();
		List<string> enemyName = new List<string>();
		string enemy_name = enemyName.Find(w => string.Equals(w,monster.name));
		if(enemy_name == null)
		enemyName.Add(monster.name);
		
		//add health to top
		Enemy mons = monster.GetComponent<Enemy>();
		health = mons.getHP();
		maxhealth += health;
		enemyHealth.Add(health);
		
		//show particle
		GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Dust")) as GameObject;
		skillparticle.transform.SetParent(transform, false);
		Destroy(skillparticle,2f);

		//enemyManager.setupMonsterName(enemytext);
        enemyManager.InitializeMaxHP(maxhealth);
        for(int i=0;i<enemyHealth.Count;i++){
            enemyManager.AddTopHP(enemyHealth[i]);
        }
		Destroy(gameObject,2f);
	}
	void Spawn ()
	{
        //amount of monster calculation
		//int max = totalEnemy + (SpawnManager.CURRENT_MODULE);
		//int min = SpawnManager.CURRENT_MODULE;
		//int amount = Random.Range(min,max);
		GameObject spawn = GameObject.FindGameObjectWithTag("SpawnManager");
        int amount = 1;
			
        EnemyManager enemyManager = spawn.GetComponent<EnemyManager>();
        //init health
        enemyManager.InitializeHP();
        float maxhealth = 0,health = 0;
        List<float> enemyHealth = new List<float>();
		List<string> enemyName = new List<string>();
		//not finished yet
		for(int i=0;i<amount;i++)
		{
			int random = Random.Range(0,monsterPrefabs.Length);
			float randomx = UnityEngine.Random.Range(-spawnRadius,spawnRadius);
			float randomz = UnityEngine.Random.Range(-spawnRadius,spawnRadius);
			//float posx = transform.position.x;
			//float posz = transform.position.z;
			
            
			
            GameObject monster = Instantiate(monsterPrefabs[random],new Vector3(randomx,0,randomz),Quaternion.identity) as GameObject;
			//monster.transform.position = transform.parent.transform.position;
			//monster.transform.SetParent(enemyManager.transform.GetChild(0),false);
			monster.transform.SetParent(transform.parent.transform,false);

			string enemy_name = enemyName.Find(w => string.Equals(w,monster.name));
			if(enemy_name == null)
            enemyName.Add(monster.name);
			
            //add health to top
			Enemy mons = monster.GetComponent<Enemy>();
            health = mons.getHP();
            maxhealth += health;
            enemyHealth.Add(health);
			
			//show particle
            GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Dust")) as GameObject;
            skillparticle.transform.SetParent(transform, false);
            Destroy(skillparticle,2f);
		}
		string enemytext = "";
		for(int i=0;i<enemyName.Count;i++)
		{
			if(i==0){
				enemytext += enemyName[0];
			}else{
				enemytext += ","+enemyName[i];
			}
		}
		enemyManager.setupMonsterName(enemytext);
        enemyManager.InitializeMaxHP(maxhealth);
        for(int i=0;i<enemyHealth.Count;i++){
            enemyManager.AddTopHP(enemyHealth[i]);
        }
		Destroy(gameObject,2f);
	}
}
