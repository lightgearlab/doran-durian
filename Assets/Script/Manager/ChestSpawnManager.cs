﻿using UnityEngine;
using System.Collections;

public class ChestSpawnManager : MonoBehaviour {
	

	GameObject chestClosed,chestOpen;
	public bool collectedChest;
	// Use this for initialization
	void Start () {
	
		chestClosed = gameObject.transform.Find("Chest").gameObject;
		chestOpen = gameObject.transform.Find("Chest2").gameObject;
		
		chestClosed.SetActive(true);
		chestOpen.SetActive(false);
	}

	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter(Collider other)
	{
		if(other.tag=="Player" && !collectedChest)
		{
            Invoke("DropDurian",1f);
        }
    }
	void DropDurian(){
		//drop durian
            Vector3 position = new Vector3(transform.position.x,transform.position.y+1,transform.position.z);
			Vector3 position2 = new Vector3(transform.position.x-2,transform.position.y+1,transform.position.z);
			Vector3 position3 = new Vector3(transform.position.x+2,transform.position.y+1,transform.position.z);
            Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position,transform.rotation);
			//Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position2,transform.rotation);
			//Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position3,transform.rotation);
			
			collectedChest=true;
			chestClosed.SetActive(false);
			chestOpen.SetActive(true);
			
			Destroy(gameObject,1f);
			
			//Invoke("spawnNext",2f);
			//spawnNext();
			 
	}
	void spawnNext(){
		//update next module
		SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
		spawnManager.spawnModule();
	}
}
