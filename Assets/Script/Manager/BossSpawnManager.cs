﻿using UnityEngine;
using System.Collections.Generic;

public class BossSpawnManager : MonoBehaviour {

	public GameObject[] monsterPrefabs;
	public float spawnRadius=5f;
	public int totalEnemy=4;
	
	void Start () {
	
		Invoke("Spawn",1f);
	}
	void Spawn ()
	{
        int amount = 1;
        GameObject spawn = GameObject.FindGameObjectWithTag("SpawnManager");
        EnemyManager enemyManager = spawn.GetComponent<EnemyManager>();
        //init health
        enemyManager.InitializeHP();
		
		
        float maxhealth = 0,health = 0;
        List<float> enemyHealth = new List<float>();
        enemyManager.setupMonsterName(monsterPrefabs[0].name);
		for(int i=0;i<amount;i++)
		{
			int random = Random.Range(0,monsterPrefabs.Length);
            //show particle
            GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Dust")) as GameObject;
			//killparticle.transform.position = new Vector3(transform.position.x+1,transform.position.y,transform.position.z-2);
            skillparticle.transform.SetParent(transform, false);
            Destroy(skillparticle,2f);
			
            GameObject monster = Instantiate(monsterPrefabs[random]) as GameObject;
			monster.transform.SetParent(transform.parent.transform,false);
            Texture blue = (Texture) Resources.Load("Material/Ubee_elektrik");
            monster.transform.GetChild(0).GetChild(1).GetComponent<Renderer>().material.SetTexture("_MainTex",blue);
            
            //add health to top
            Enemy mons = monsterPrefabs[random].GetComponent<Enemy>();
            health = mons.getHP();
            maxhealth += health;
            enemyHealth.Add(health);
		}
        enemyManager.InitializeMaxHP(maxhealth);
        for(int i=0;i<enemyHealth.Count;i++){
            enemyManager.AddTopHP(enemyHealth[i]);
        }
		Destroy(gameObject,2f);
	}
}
