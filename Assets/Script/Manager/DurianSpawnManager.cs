﻿using UnityEngine;
using System;
using System.Collections;

public class DurianSpawnManager : MonoBehaviour
{
	public GameObject durian;                // The enemy prefab to be spawned.
	public float spawnTime = 3f;            // How long between each spawn.
	public Transform[] spawnPoints;         // An array of the spawn points this enemy can spawn from.
	public static int durianNum = 0;
	public int durianMax = 5;
	public int totalDurian;
	public float durianTimer;
	public float durianTimerMax = 10f;
	public float spawnRadius = 2f;

	
	void SpawnDurian(){
		
		//add radius for random instantiate
		float randomx = UnityEngine.Random.Range(-spawnRadius,spawnRadius);
		float randomz = UnityEngine.Random.Range(-spawnRadius,spawnRadius);
		float posx = transform.position.x + randomx;
		float posz = transform.position.z + randomz;
		
		Vector3 newpos = new Vector3(posx,transform.position.y+3,posz);
		GameObject durianObject = (GameObject)Instantiate(durian,newpos,Quaternion.identity);
		//play particle
		ParticleSystem particle = durianObject.GetComponent<ParticleSystem>();
		particle.Play();
		
		durianNum++;   // durian counter
		
	}
}