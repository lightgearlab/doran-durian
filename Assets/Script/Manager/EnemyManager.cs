﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class EnemyManager : MonoBehaviour {

    float currentHealth;
    bool isAlive = false;
	AudioSource audioSource;
	void Start () {
        audioSource = gameObject.AddComponent<AudioSource>();
	}
	public void InitializeHP(){
        currentHealth = 0;
        setupSliderText(currentHealth);
    }
    public void InitializeMaxHP(float max){
        //hp_slider.maxValue = max;
        //setupSliderText(max);
        isAlive = true;
    }
    public void AddTopHP(float health){
        currentHealth += health;
        setupSliderText(currentHealth);
    }
    public void setupMonsterName(string name){
        //name_text.text = name;
    }
    public void TakeDamage(float damage){
        audioSource.clip = Resources.Load<AudioClip>("Audio/Attack");
        audioSource.Play();
        
        
        currentHealth -= damage;
        if(currentHealth <= 0 && isAlive){
           currentHealth = 0;
           setupSliderText(0);
           isAlive = !isAlive;
           Invoke("spawnNext",1f);
        }else{
            setupSliderText(currentHealth);
        }
        
    }
    public void TakeDamageBoss(float damage){
        audioSource.clip = Resources.Load<AudioClip>("Audio/Attack");
        audioSource.Play();
        
        currentHealth -= damage;
        if(currentHealth <=0 && isAlive){
            currentHealth = 0;
            setupSliderText(0);
            isAlive = !isAlive;
            Invoke("spawnNext",1f);
        }else{
            setupSliderText(currentHealth);
        }
        
    }
    void spawnNext(){
         //spawn
        SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
        spawnManager.spawnModule();
    }
    void setupSliderText(float health){
        //hp_slider.value = health;
        //hp_text.text = ""+ health;
    }
}
