﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
public class StopPoint : MonoBehaviour {

	public List<Transform> cameraPos;
	public int pos = 0;
	public int stopPointNumber;
	public Transform left,right,down,up;
	TurnManager turnManager;
	DoranPlayer doranPlayer;
	public CanvasScript mainCanvas;
	public string ownedBy;
	public int timeToHarvest;
	DoranState state;
	DialogPanelScript dialogPanel;
	public enum StopTypes {
        START,
        MONSTER_OR_EVENT,
		BOSS,
        ITEM,
        WEAPON,
		ITEMSHOP,
		WEAPONSHOP,
		FIRE,
		ICE,
		THUNDER,
		LEAF,
		FIRE_ELEM,
		ICE_ELEM,
		THUNDER_ELEM,
		LEAF_ELEM,
		EMPTY,
		FARMING,
		SPECIAL,
		HOME,
        END
    }
	public StopTypes stopType;
	public GameObject currentPlayer;

	// Use this for initialization
	void Start () {
		mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<CanvasScript>();
		turnManager = GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>();
		dialogPanel = mainCanvas.dialogPanel.GetComponent<DialogPanelScript>();
		
		setupStopPoint();
	}

	public void setMoveset(GameObject player){
		//Debug.Log("set moveset for "+currentPlayer.name);
		currentPlayer = player;
		state = currentPlayer.GetComponent<DoranState>();
		doranPlayer = currentPlayer.GetComponent<DoranPlayer>();

		state.move_left = left;
		state.move_right = right;
		state.move_down = down;
		state.move_up = up;
	}
	public void removeMoveset(GameObject player){
		currentPlayer = player;
		state = currentPlayer.GetComponent<DoranState>();
		doranPlayer = currentPlayer.GetComponent<DoranPlayer>();

		state.move_left = null;
		state.move_right = null;
		state.move_down = null;
		state.move_up = null;
	}

	void OnTriggerEnter(Collider other) {
       if(other.tag == "Player"){

		    currentPlayer = other.gameObject;

		    playerPassThrough();
		   
		    //setup stop points when end of movement
			doranPlayer = other.GetComponent<DoranPlayer>();

			state = other.GetComponent<DoranState>();
			state.currentSpot = gameObject;
			state.traveledPoints.Add(transform);
			state.addRemoveMoveCount(-1);
			//Debug.Log("Set move"+state.moveCount);
			if(state.moveCount>0){
				//setMoveset();
				
			}else{
				//out of move count
				//can reverse
				//removeMoveset();

				//check panel
				if(stopType == StopTypes.EMPTY && !doranPlayer.isEmptyElem()){

					plantSeed(other);

				}else if(stopType == StopTypes.FIRE ||stopType == StopTypes.ICE||
						stopType == StopTypes.THUNDER ||stopType == StopTypes.LEAF
						){

					if(!doranPlayer.isEmptySpot())
						upgradePlant(other);
					else{
						spawnStopPointAndEndTurn(other);
						SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
					}

				}else{
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
				}
				
			}
		   		
			//disable change to idle for the first time
			if(state.currentState == DoranState.State.MOVING)
				state.changeStateAndExecuteTurn(DoranState.State.DICE_MOVE);
			else{
				//Debug.Log("Error");
				//state.changeStateAndExecuteTurn(DoranState.State.DICE_MOVE);
			}
		   
		}
	}
	void plantSeed(Collider other){
		dialogPanel.showDialog();
		dialogPanel.setText("Plant seed here?");
		dialogPanel.yesButton.onClick.AddListener(delegate {

			dialogPanel.closeDialog();
			dialogPanel.showDialog();
			dialogPanel.setText("Choose which seed?");
			dialogPanel.showSeeds(doranPlayer.player);
			dialogPanel.fireButton.interactable = true;
			dialogPanel.iceButton.interactable = true;
			dialogPanel.thunderButton.interactable = true;
			dialogPanel.leafButton.interactable = true;
			if(doranPlayer.isEmptyElem(StopTypes.FIRE_ELEM)){
				dialogPanel.fireButton.interactable = false;
			}
			if(doranPlayer.isEmptyElem(StopTypes.ICE_ELEM)){
				dialogPanel.iceButton.interactable = false;
			}
			if(doranPlayer.isEmptyElem(StopTypes.THUNDER_ELEM)){
				dialogPanel.thunderButton.interactable = false;
			}
			if(doranPlayer.isEmptyElem(StopTypes.LEAF_ELEM)){
				dialogPanel.leafButton.interactable = false;
			}
			dialogPanel.fireButton.onClick.AddListener(delegate {
					doranPlayer.addElem(-1,StopTypes.FIRE);
					convertSpot(state.currentSpot,StopTypes.FIRE);
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
			});
			dialogPanel.iceButton.onClick.AddListener(delegate {
					doranPlayer.addElem(-1,StopTypes.ICE);
					convertSpot(state.currentSpot,StopTypes.ICE);
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
			});
			dialogPanel.thunderButton.onClick.AddListener(delegate {
					doranPlayer.addElem(-1,StopTypes.THUNDER);
					convertSpot(state.currentSpot,StopTypes.THUNDER);
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
			});
			dialogPanel.leafButton.onClick.AddListener(delegate {
					doranPlayer.addElem(-1,StopTypes.LEAF);
					convertSpot(state.currentSpot,StopTypes.LEAF);
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
			});
			
		});
		dialogPanel.noButton.onClick.AddListener(delegate {

			dialogPanel.closeDialog();
			spawnStopPointAndEndTurn(other);
			SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);

		});
	}
	void upgradePlant(Collider other){
			dialogPanel.showDialog();
			dialogPanel.setText("Upgrade plants?");
			dialogPanel.yesButton.onClick.AddListener(delegate {

				dialogPanel.closeDialog();
				dialogPanel.showDialog();
				dialogPanel.setText("Choose which plant?");
				dialogPanel.showSpots(doranPlayer.player);
				dialogPanel.fireButton.interactable = true;
				dialogPanel.iceButton.interactable = true;
				dialogPanel.thunderButton.interactable = true;
				dialogPanel.leafButton.interactable = true;
				if(doranPlayer.isEmptySpot(StopTypes.FIRE)){
					dialogPanel.fireButton.interactable = false;
				}
				if(doranPlayer.isEmptySpot(StopTypes.ICE)){
					dialogPanel.iceButton.interactable = false;
				}
				if(doranPlayer.isEmptySpot(StopTypes.THUNDER)){
					dialogPanel.thunderButton.interactable = false;
				}
				if(doranPlayer.isEmptySpot(StopTypes.LEAF)){
					dialogPanel.leafButton.interactable = false;
				}
				dialogPanel.fireButton.onClick.AddListener(delegate {
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
				});
				dialogPanel.iceButton.onClick.AddListener(delegate {
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
				});
				dialogPanel.thunderButton.onClick.AddListener(delegate {
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
				});
				dialogPanel.leafButton.onClick.AddListener(delegate {
					dialogPanel.closeDialog();
					spawnStopPointAndEndTurn(other);
					SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
				});
				
			});
			dialogPanel.noButton.onClick.AddListener(delegate {
				dialogPanel.closeDialog();
				spawnStopPointAndEndTurn(other);
				SaveInPlayerPref.saveCurrentPlayerLocation(turnManager.playerList);
			});
	}
	void convertSpot(GameObject spot,StopTypes type){
		//Debug.Log("converting spot " + spot.name);
		spot.GetComponent<StopPoint>().ownedBy = doranPlayer.name;
		doranPlayer.addOwnedSpot(spot.GetComponent<StopPoint>());
		//spot.GetComponent<StopPoint>().stopType = StopTypes.FARMING;
		spot.GetComponent<StopPoint>().stopType = type;
		switch(type){
			case StopTypes.FIRE:
			spot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = dialogPanel.fireButton.transform.GetChild(0).GetComponent<Image>().sprite;
			spot.GetComponent<StopPoint>().timeToHarvest = 2;
			break;
			case StopTypes.ICE:
			spot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = dialogPanel.iceButton.transform.GetChild(0).GetComponent<Image>().sprite;
			spot.GetComponent<StopPoint>().timeToHarvest = 2;
			break;
			case StopTypes.THUNDER:
			spot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = dialogPanel.thunderButton.transform.GetChild(0).GetComponent<Image>().sprite;
			spot.GetComponent<StopPoint>().timeToHarvest = 3;
			break;
			case StopTypes.LEAF:
			spot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = dialogPanel.leafButton.transform.GetChild(0).GetComponent<Image>().sprite;
			spot.GetComponent<StopPoint>().timeToHarvest = 3;
			break;
		}
		turnManager.farmedPoints.Add(spot);
	}
	//to be called when turn ends
	public void minusTimeToHarvest(GameObject selfSpot){
		
		if(timeToHarvest <= 0){
			//auto harvest and add buah.
			foreach(GameObject player in turnManager.playerList){
				if(player.name.Equals(selfSpot.GetComponent<StopPoint>().ownedBy)){
					player.GetComponent<DoranPlayer>().addBuah(1,selfSpot.GetComponent<StopPoint>().stopType);
					player.GetComponent<DoranPlayer>().removeOwnedSpot(selfSpot.GetComponent<StopPoint>());
				}
			}
			//Debug.Log(currentPlayer.name + "-own name=" + selfSpot.GetComponent<StopPoint>().ownedBy);
			selfSpot.GetComponent<StopPoint>().ownedBy = "";
			selfSpot.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite =  ImageManager.getSpriteFromSpriteSheet("UI/gamegui","gamegui_14");
			selfSpot.GetComponent<StopPoint>().stopType = StopTypes.EMPTY;
			//Debug.Log(selfSpot.name+" Expired");
		}else{
			timeToHarvest--;
			//Debug.Log(selfSpot.name+" ->"+timeToHarvest);
		}
	}
	void playerPassThrough(){
		
		if(currentPlayer.GetComponent<DoranState>().currentState == DoranState.State.MOVING){
			switch(stopType){
				case StopTypes.FIRE_ELEM:
				if(!currentPlayer.GetComponent<DoranPlayer>().checkPlayerHasFruit(StopTypes.FIRE)){
					gameObject.transform.GetChild(1).GetComponent<Animator>().SetTrigger("attack");		
					currentPlayer.GetComponent<DoranPlayer>().setHasFruit(StopTypes.FIRE);
				}
				break;
				case StopTypes.ICE_ELEM:
				if(!currentPlayer.GetComponent<DoranPlayer>().checkPlayerHasFruit(StopTypes.ICE)){
					gameObject.transform.GetChild(1).GetComponent<Animator>().SetTrigger("attack");	
					currentPlayer.GetComponent<DoranPlayer>().setHasFruit(StopTypes.ICE);
				}
				break;
				case StopTypes.THUNDER_ELEM:
				if(!currentPlayer.GetComponent<DoranPlayer>().checkPlayerHasFruit(StopTypes.THUNDER)){
					gameObject.transform.GetChild(1).GetComponent<Animator>().SetTrigger("attack");
					currentPlayer.GetComponent<DoranPlayer>().setHasFruit(StopTypes.THUNDER);
				}
				break;
				case StopTypes.LEAF_ELEM:
				if(!currentPlayer.GetComponent<DoranPlayer>().checkPlayerHasFruit(StopTypes.LEAF)){
					gameObject.transform.GetChild(1).GetComponent<Animator>().SetTrigger("attack");
					currentPlayer.GetComponent<DoranPlayer>().setHasFruit(StopTypes.LEAF);
				}
				break;
				case StopTypes.HOME:
				if(currentPlayer.GetComponent<DoranPlayer>().checkPlayerHasAllFruit()){
					currentPlayer.GetComponent<DoranPlayer>().removeAllFruit();
					//level up
					currentPlayer.GetComponent<DoranPlayer>().addDurian(1);
					currentPlayer.GetComponent<DoranPlayer>().addElem(1,StopTypes.FIRE);
					currentPlayer.GetComponent<DoranPlayer>().addElem(1,StopTypes.ICE);
					currentPlayer.GetComponent<DoranPlayer>().addElem(1,StopTypes.THUNDER);
					currentPlayer.GetComponent<DoranPlayer>().addElem(1,StopTypes.LEAF);

				}
				break;
			}
		}else{
			
		}
		
	}
	void setupStopPoint(){
		GameObject point;
		switch(stopType){
			case StopTypes.START:
			gameObject.transform.GetChild(0).GetChild(0).
			GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("Upgrade_learn_planting");
			break;

			case StopTypes.MONSTER_OR_EVENT:
			
			
			break;

			case StopTypes.BOSS:
			
			break;

			case StopTypes.ITEM:
			gameObject.transform.GetChild(0).GetChild(0).
			GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("Upgrade_learn_shop");
			break;

			case StopTypes.WEAPON:
			gameObject.transform.GetChild(0).GetChild(0).
			GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("Upgrade_learn_shop");
			break;

			case StopTypes.ITEMSHOP:
			break;

			case StopTypes.WEAPONSHOP:
			break;

			case StopTypes.END:
			break;

			case StopTypes.FIRE_ELEM:
			point = Instantiate(Resources.Load("Prefabs/Chilli")) as GameObject;
			//point.transform.position = transform.position;
			point.transform.SetParent(transform,false);
			break;
			case StopTypes.ICE_ELEM:
			point = Instantiate(Resources.Load("Prefabs/Coconut")) as GameObject;
			//point.transform.position = transform.position;
			point.transform.SetParent(transform,false);
			break;
			case StopTypes.THUNDER_ELEM:
			point = Instantiate(Resources.Load("Prefabs/Onion")) as GameObject;
			//point.transform.position = transform.position;
			point.transform.SetParent(transform,false);
			break;
			case StopTypes.LEAF_ELEM:
			point = Instantiate(Resources.Load("Prefabs/JackFruit")) as GameObject;
			//point.transform.position = transform.position;
			point.transform.SetParent(transform,false);
			break;
		}
	}

	void spawnStopPointAndEndTurn(Collider other){
		//Debug.Log("Spawn");
		GameObject point;
		switch(stopType){
			case StopTypes.START:
			break;

			case StopTypes.MONSTER_OR_EVENT:
			point = Instantiate(Resources.Load("Prefabs/Manager/MonsterSpawnManager")) as GameObject;
			point.transform.position = transform.position;
			point.transform.SetParent(transform);
			
			break;

			case StopTypes.BOSS:
			point = Instantiate(Resources.Load("Prefabs/Manager/MonsterSpawnManager")) as GameObject;
			point.transform.position = transform.position;
			point.transform.SetParent(transform);
			break;

			case StopTypes.ITEM:
			point = Instantiate(Resources.Load("Prefabs/Manager/ChestManager")) as GameObject;
			point.transform.position = transform.position;
			point.transform.SetParent(transform);
			break;

			case StopTypes.WEAPON:
			point = Instantiate(Resources.Load("Prefabs/Manager/ChestManager")) as GameObject;
			point.transform.position = transform.position;
			point.transform.SetParent(transform);
			break;

			default:
			StartCoroutine(other.GetComponent<DoranState>().EndTurn(1));
			break;
			
		}
	}
}
