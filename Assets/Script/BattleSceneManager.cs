﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BattleSceneManager : MonoBehaviour {

	public GameObject choices;
	public Text round_text;
	public Text player_choice;
	int current_round = 0;
	public int player1_choice,player2_choice;
	public string choicep1,choicep2;
	public int player1_win,player2_win;
	public GameObject player1,player2;
	// Use this for initialization
	void Start () {
		ChangeScene.startScene();
		//check which player battle
		Debug.Log(PlayerPrefs.GetString("battlePlayer")+" is battling");
		addCurrentRound();
	}
	// Update is called once per frame
	void Update () {
		
	}
	void addCurrentRound(){
		current_round++;
		round_text.text = "Round "+current_round + "    Player 1:"+player1_win + " Player 2:"+player2_win;
		player_choice.text = "Player 1:" + choicep1 + "  " + "Player 2:" + choicep2 ;
		showChoices();
	}
	void showChoices(){
		//choices.SetActive(true);
		choices.GetComponent<Animator>().SetTrigger("show");
	}
	public void hideChoices(){
		//choices.SetActive(false);
	}
	
	void PlayerAttack(){
		player1.transform.GetChild(0).GetComponent<Animator>().SetTrigger("IsAttackingRandom");
	}
	void PlayerDie(){
		player1.transform.GetChild(0).GetComponent<Animator>().SetTrigger("IsDead");
	}
	void EnemyAttack(){
		player2.GetComponent<Animator>().SetTrigger("IsAttacking");
	}
	void EnemyDie(){
		player2.GetComponent<Animator>().SetTrigger("Die");
	}
	public void chosenChoice(int choice){
		switch(choice){
			case 0:
			Debug.Log("Player choose Rock");
			choicep1 = "Rock";
			break;

			case 1:
			Debug.Log("Player choose Paper");
			choicep1 = "Paper";
			break;

			case 2:
			Debug.Log("Player choose Scissor");
			choicep1 = "Scissor";
			break;
		}
		player1_choice = choice;
		
		choices.GetComponent<Animator>().SetTrigger("hide");
		//show animations
		StartCoroutine(enemyChoice());
	}
	private IEnumerator enemyChoice(){
		 yield return new WaitForSeconds(1f);
			int  choice = Random.Range(0,3);
			switch(choice){
				case 0:
				Debug.Log("Enemy choose Rock");
				choicep2 = "Rock";
				break;

				case 1:
				Debug.Log("Enemy choose Paper");
				choicep2 = "Paper";
				break;

				case 2:
				Debug.Log("Enemy choose Scissor");
				choicep2 = "Scissor";
				break;
			}
			player2_choice = choice;
			calculateRound();
	}

	public void calculateRound(){
		//Debug.Log("calculating .. ");
		if(player1_choice == 0 && player2_choice == 0){
			//Debug.Log("Draw");
		}
		if(player1_choice == 0 && player2_choice == 1){
			//Debug.Log("Enemy wins");
			EnemyAttack();
			player2_win++;
		}
		if(player1_choice == 0 && player2_choice == 2){
			//Debug.Log("Player wins");
			PlayerAttack();
			player1_win++;
		}

		if(player1_choice == 1 && player2_choice == 0){
			//Debug.Log("Player wins");
			PlayerAttack();
			player1_win++;
		}
		if(player1_choice == 1 && player2_choice == 1){
			//Debug.Log("Draw");
		}
		if(player1_choice == 1 && player2_choice == 2){
			//Debug.Log("Enemy wins");
			EnemyAttack();
			player2_win++;
		}

		if(player1_choice == 2 && player2_choice == 0){
			//Debug.Log("Enemy wins");
			EnemyAttack();
			player2_win++;
		}
		if(player1_choice == 2 && player2_choice == 1){
			//Debug.Log("Player wins");
			PlayerAttack();
			player1_win++;
		}
		if(player1_choice == 2 && player2_choice == 2){
			//Debug.Log("Draw");
		}
		if(player1_win > 2 || player2_win > 2){
			if(player1_win > 2){
			    PlayerPrefs.SetString("mainPlayerResult","Win");
				EnemyDie();
			}
			if(player2_win > 2){
				PlayerPrefs.SetString("mainPlayerResult","Lose");
				PlayerDie();
			}
			StartCoroutine(changeScene());
		}else{
			addCurrentRound();
		}
		
	}
	private IEnumerator changeScene(){
		yield return new WaitForSeconds(2f);
			ChangeScene.playAnimation();
	}
}
