﻿using UnityEngine;
using System.Collections;

public class EnemyAttack : MonoBehaviour {

    GameObject spawnManager;
    BattleManager battleManager;
    Enemy enemy;
    public float cooldown_atk = 3f;
    public float timer;
	Animator anim;
	// Update is called once per frame
	void Update () {
	    //timer for cooldown
        timer += Time.deltaTime;
	}
    void Start(){
        anim = transform.parent.GetComponent<Animator>();
        battleManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
    }
    public void Attack(){
        
        GameObject target = GameObject.FindGameObjectWithTag("Player");
        if (target != null) {
            transform.parent.LookAt (target.transform.position);
            anim.SetTrigger("IsAttacking");
            target.GetComponent<PlayerHealth>().TakeDamage(transform.parent.GetComponent<Enemy>().getATK());
            StartCoroutine(battleManager.endTurn());
        }
        
    }
        
    public void Attack_Normal(GameObject enemy,GameObject target){
        
        Animator anim = enemy.GetComponent<Animator>();
        if (timer >= cooldown_atk )
        {
            if(!anim.GetBool("IsAttacking"))
            {
                anim.SetTrigger("IsAttacking");
                if (target != null) {
                    //default enemy damage, should minus defence
                    target.GetComponent<PlayerHealth>().TakeDamage(enemy.GetComponent<Enemy>().getATK());
                    //attack
                    //anim.SetFloat("Attack",random);
                    timer = 0;
                    //sound
                    //AudioSource.PlayClipAtPoint (attackClip, gameObject.transform.position);
                }   
            }

        }
    }
}
