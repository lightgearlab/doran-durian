﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.AI;
public class Enemy : MonoBehaviour {

    public int id;
    public float maxHealth = 100f;
    public int ATK_i;
    public bool isDamaged = false;
    float currentHealth;
    float attackRange = 8f;
    int ATK;
    bool isAlive = false;
    Animator anim;
    NavMeshAgent nav;
    GameObject spawnManager;
    GameObject player;
    
    public enum EnemyType
    {
        START,
        NORMAL,
        BOSS
    }
    public enum EnemyState
    {
        IDLE,
        ATTACK,
        DEFENCE,
        SKILL
    }
    public EnemyType enemyType;
    public EnemyState enemyState;
    
	// Use this for initialization
	void Start () {
        anim =  GetComponent<Animator>();
        nav = GetComponent<NavMeshAgent>();
        nav.enabled = true;
        player = GameObject.FindGameObjectWithTag("TurnManager")
		.GetComponent<TurnManager>()
		.playerList[0];

        initializeEnemy();
	}
	public void initializeEnemy(){
        isAlive = true;
        getHP();
        getATK();

        enemyState = EnemyState.IDLE;
    }
    public float getATK(){
        int temp = ATK_i;
        switch (enemyType)
        {
            case EnemyType.START:
            break;
            
            case EnemyType.NORMAL:
            temp += (spawnManager.GetComponent<SpawnManager>().CURRENT_DEPTH-1);
            break;
            
            case EnemyType.BOSS:
            temp += ((spawnManager.GetComponent<SpawnManager>().CURRENT_DEPTH-1) * 2);
            break;
            
            default:
            break;
        }
        ATK = temp;
        return ATK;
    }
    public float getHP(){
        spawnManager = GameObject.FindGameObjectWithTag("SpawnManager");
        float HP = maxHealth;
        switch (enemyType)
        {
            case EnemyType.START:
            //HP += (spawnManager.GetComponent<SpawnManager>().CURRENT_DEPTH * 50);
            break;
            
            case EnemyType.NORMAL:
            HP += ((spawnManager.GetComponent<SpawnManager>().CURRENT_DEPTH-1) * 50);
            break;
            
            case EnemyType.BOSS:
            HP += ((spawnManager.GetComponent<SpawnManager>().CURRENT_DEPTH-1) * 50);
            break;
            
            default:
            break;
        }
        currentHealth = HP;
        return currentHealth;
    }
	// Update is called once per frame
	void Update () {
        if(isAlive){
            switch (enemyState)
            {
                case EnemyState.IDLE:
                gameObject.transform.LookAt (player.transform.position);
                break;
                
                case EnemyState.ATTACK:
                Chasing(player);
                break;
            }
        }
            
	}
    
    public void Chasing(GameObject player)
	{
        transform.LookAt (player.transform.position);
        if(player!=null){
            //if within attack range, attack!
            float distance = Vector3.Distance(player.transform.position,transform.position);
            if(distance < attackRange){
                //if enemy is not damaged and still alive, chase
                if (currentHealth > 0 && !isDamaged)
                {
                    if(distance < 3){
                        
                        //nav.SetDestination(gameObject.transform.position); //stay still
                        nav.Stop();
                        GetComponent<Animator>().SetBool("Walking", false);
                        
                        //initiate enemy attack
                        EnemyAttack enemyAttack = transform.GetChild(2).GetComponent<EnemyAttack>();
                        if(enemyAttack!= null){
                            enemyAttack.Attack_Normal(gameObject,player);
                        }
                       
                    
                    }else{
                        nav.Resume();
                        nav.SetDestination(player.transform.position);   // towards player
                        GetComponent<Animator>().SetBool("Walking", true);
                    }
                }
                else {
                    nav.Stop();
                    //nav.SetDestination(gameObject.transform.position); //stay still
                }
            }
        }
	}
    public void TakeDamageFairy(GameObject enemy,float damage){
        if(spawnManager != null)
            spawnManager.GetComponent<EnemyManager>().TakeDamage(damage);
        currentHealth -= damage;
        transform.GetChild(1).GetComponent<DamagePopups>().CreateDamagePopup((int)damage);
        
        
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Spark")) as GameObject;
        skillparticle.transform.SetParent(gameObject.transform, false);
        Destroy(skillparticle,2f);
        
        checkDie(currentHealth);
    }
    public void TakeDamageFairyBoss(GameObject enemy,float damage){
        if(spawnManager != null)
            spawnManager.GetComponent<EnemyManager>().TakeDamageBoss(damage);
        currentHealth -= damage;
        transform.GetChild(1).GetComponent<DamagePopups>().CreateDamagePopup((int)damage);
        
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Spark")) as GameObject;
        skillparticle.transform.SetParent(gameObject.transform, false);
        Destroy(skillparticle,2f);
        
        checkDieBoss(currentHealth);
    }
    public void TakeDamage(GameObject player,float damage){
        if(spawnManager != null)
            spawnManager.GetComponent<EnemyManager>().TakeDamage(damage);
        currentHealth -= damage;

        Slider sp = transform.GetChild(1).GetChild(0).GetComponent<Slider>();
        sp.value = (currentHealth / maxHealth);

        transform.GetChild(1).GetComponent<DamagePopups>().CreateDamagePopup((int)damage);
        
        //show particle
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Blast")) as GameObject;
        skillparticle.transform.SetParent(gameObject.transform, false);
        Destroy(skillparticle,2f);
        //die
        checkDie(currentHealth);
        
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
    public void TakeDamageBoss(GameObject player,float damage){
        if(spawnManager != null)
            spawnManager.GetComponent<EnemyManager>().TakeDamageBoss(damage);
        currentHealth -= damage;
        transform.GetChild(1).GetComponent<DamagePopups>().CreateDamagePopup((int)damage);
        
        //show particle
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Blast")) as GameObject;
        skillparticle.transform.SetParent(gameObject.transform, false);
        Destroy(skillparticle,2f);
        //die
        checkDieBoss(currentHealth);
        
        AudioSource audio = GetComponent<AudioSource>();
        audio.Play();
    }
    void checkDie(float currentHealth){
        if(currentHealth <= 0 && isAlive){
            
            isAlive = !isAlive;
            GetComponent<SphereCollider>().enabled = false;
            currentHealth = 0;
            GetComponent<AudioSource>().Play();
            anim.SetTrigger("Die");
            tag = "Finish";
            //drop durian
            Vector3 position = new Vector3(transform.position.x,transform.position.y,transform.position.z+2);
            Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position,transform.rotation);
            GetComponent<SphereCollider>().enabled = false;
            Destroy(gameObject,2f);
            //add monster killed count
            player.GetComponent<DoranPlayer>().addMonsterKilledCount(id);
        }
    }
    
    void checkDieBoss(float currentHealth){
        if(currentHealth <= 0 && isAlive){
            isAlive = !isAlive;
            GetComponent<SphereCollider>().enabled = false;
            currentHealth = 0;
            GetComponent<AudioSource>().Play();
            anim.SetTrigger("Die");
            tag = "Finish";
            //drop durian
            Vector3 position = new Vector3(transform.position.x,transform.position.y,transform.position.z+2);
            Vector3 position2 = new Vector3(transform.position.x+1,transform.position.y,transform.position.z+2);
            Vector3 position3 = new Vector3(transform.position.x-1,transform.position.y,transform.position.z+2);
            
            Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position,transform.rotation);
            Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position2,transform.rotation);
            Instantiate(Resources.Load("Prefabs/SingleDurian",typeof(GameObject)),position3,transform.rotation);
            GetComponent<SphereCollider>().enabled = false;
            Destroy(gameObject,1f);
        }
    }
}
