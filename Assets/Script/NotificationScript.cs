﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;

public class NotificationScript : MonoBehaviour {

    
	public float timer = 0;
    float cooldown = 10f;
    static bool isShowing = false;
    bool isShowRandom = false;
    List<string> help_text = new List<string>();
        
	void Start () {
	    //notification script
        help_text.Add("Tap the screen to attack");
        help_text.Add("Press the triangle to show upgrades available");
        help_text.Add("Tap anywhere now! Help doran!");
        help_text.Add("Press the Attack or Heal button to change mode!");
        help_text.Add("You can press the fairy to change mode!");
        
        
        //showNotification(help_text[0]);
	}
	void Update () {
        if(isShowRandom){
            timer += Time.deltaTime;
            if(timer > cooldown)
                showRandomNotify();
        }
	}
    public void showRandom(bool show){
        isShowRandom = show;
    }
    public void showRandomNotify(){
        //int r = Random.Range(0,help_text.Count);
        if(!isShowing){
            //showNotification(help_text[r]);
            timer = 0;
        }
        
    }
	public static void showNotification(string text){
        //Debug.Log("show "+text);
        isShowing = true;
        
        GameObject notifi = GameObject.FindGameObjectWithTag("Notification");
        Text notification_text = notifi.transform.GetChild(0).GetComponent<Text>();
        
        notifi.GetComponent<Image>().enabled = true;
        notification_text.enabled = true;
        
        notifi.GetComponent<Animator>().SetTrigger("show");
        notification_text.text = text;
    }
    public void disableNotification(){
        GameObject notifi = GameObject.FindGameObjectWithTag("Notification");
        Text notification_text = notifi.transform.GetChild(0).GetComponent<Text>();
        
        notifi.GetComponent<Image>().enabled = false;
        notification_text.enabled = false;
        
        isShowing = false;
    }
}
