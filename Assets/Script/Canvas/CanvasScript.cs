﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class CanvasScript : MonoBehaviour {

    GameObject upgradepanel,upgradewindow,menuwindow,maincanvas;
    public Text fire_amount,ice_amount,leaf_amount,thunder_amount;
    public Text amount_cili,amount_kelapa,amount_nangka,amount_bawang;
    public Image img_cili,img_kelapa,img_nangka,img_bawang;
    public bool window_open = false;
    bool menu_open = false;
    
    //upgrade page
    public static int CURRENT_PAGE = 0;
    public static int NO_PAGE = -1;
    public static int PAGE_DORAN = 0;
    public static int PAGE_FAIRY = 1;
    public static int PAGE_MONSTER = 2;
    public static int PAGE_SHOP = 3;
    public static int PAGE_PLANT = 4;
    
    //canvas panel
    public static int DURIAN = 0;
    public static int HP_MONSTER = 1;
    public static int FAIRY_MODE = 2;
    public static int SKILL = 3;
    public static int UPGRADE = 4;
    public static int TXT_FPS = 6;
    public static int TXT_DEPTH = 7;
    public static int MENU = 7;
    public static int CHANGE_SCENE = 10;
    public static int BATTLE_MODE = 11;

    //upgrade panel
    public static int PROGRESS_BAR = 0;
    public static int PANEL = 1;
    public static int BTN_UPGRADE = 5;
    public GameObject dialogPanel;
    DoranPlayer player;
    bool TEXT_PANEL = false;
    //not using yet
    public enum Skill {
        Fairy_ATK,
        FAIRY_HEAL,
        NOT_USED
    }
	void Start () {
        
	    initialze();
        
        //showDoranUpgradeUI();
        //CURRENT_PAGE = NO_PAGE;
        
        //check skill list and add to skill panel
        // if(player.player.baseSkill.Count > 0){
        //     for(int i=0;i<player.player.baseSkill.Count;i++){
        //         BaseSkill skill = player.player.baseSkill[i];
        //         addSkilltoPanel(i,skill);
        //     }
        // }
        
        //upgradepanel.transform.GetChild(2).GetComponent<Image>().color = new Color32(223,49,79,255);
	}
    public void initialze(){
        upgradepanel = GameObject.FindGameObjectWithTag("UpgradePanel");
        upgradewindow = GameObject.FindGameObjectWithTag("UpgradeWindow");
        maincanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        menuwindow = maincanvas.transform.GetChild(MENU).gameObject;
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<DoranPlayer>();
    }
	public void openPanel(){
        upgradepanel = GameObject.FindGameObjectWithTag("UpgradePanel");
        if(!window_open){
            upgradepanel.GetComponent<Animator>().SetBool("enable",true);
            upgradepanel.transform.GetChild(BTN_UPGRADE).GetComponent<Animator>().SetBool("menu_open",true);
        }  
        else{
            upgradepanel.GetComponent<Animator>().SetBool("enable",false);
            upgradepanel.transform.GetChild(BTN_UPGRADE).GetComponent<Animator>().SetBool("menu_open",false);
        }
        window_open = !window_open;
            
    }
    public void openMenu(){
        if(!menu_open){
            menuwindow.SetActive(true);
        }  
        else{
            menuwindow.SetActive(false);
        }
        menu_open = !menu_open;
    }
    
    
    public void refreshContent(){
        //Debug.Log("refreshing upgrade page");
       //if(CURRENT_PAGE != NO_PAGE)
       removeContent(CURRENT_PAGE);
    }
    public void removeContent(int num){
        //Debug.Log("current page: "+CURRENT_PAGE);
        if(GameObject.FindGameObjectsWithTag("List").Length > 0){
            GameObject[] gameobjects = GameObject.FindGameObjectsWithTag("List");
            for(int i=0;i < (gameobjects.Length);i++){
                Destroy(gameobjects[i]);
            }
        }
        //refresh page color
        upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_DORAN).GetComponent<Image>().color = new Color32(163,117,101,255);
        upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_FAIRY).GetComponent<Image>().color = new Color32(163,117,101,255);
        upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_MONSTER).GetComponent<Image>().color = new Color32(163,117,101,255);
        upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_SHOP).GetComponent<Image>().color = new Color32(163,117,101,255);
        upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_PLANT).GetComponent<Image>().color = new Color32(163,117,101,255);
        
        switch (num)
        {
            case 0:
            CURRENT_PAGE = PAGE_DORAN;
            showDoranUpgradeUI();
            break;
            
            case 1:
            CURRENT_PAGE = PAGE_FAIRY;
            showPlayerUpgradeUI();
            break;
            
            case 2:
            CURRENT_PAGE = PAGE_MONSTER;
            showMonsterUI();
            break;
            
            default:
            break;
        }
        upgradepanel.transform.GetChild(PANEL).GetChild(0).transform.GetChild(num).GetComponent<Image>().color = new Color32(223,49,79,255);
    }
    public void showPlayerUpgradeUI(){
        //9
        int size = 2;
        for(int i=1;i<=size;i++){
            
            BaseSkill panel = new BaseSkill();
            int current_durian = player.GetComponent<DoranPlayer>().player.currentDurian;

            switch (i)
            {
                //skill
                case 1:
                panel.Name = "THE DARK HARVEST";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "100 DMG to all enemy";
                panel.Description = "Drop a bountiful of harvested fruits to enemy";
                panel.skillCost = 100;
                panel.skillPreAmount = 100;
                panel.image = "Fairy_skill_magic";
                if(SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_1) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
               
                break;
                
                case 2:
                panel.Name = "MEGA HEAL";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "100HP heal";
                panel.Description = "The goddess descends to bless the adventurer";
                panel.skillCost = 20;
                panel.skillPreAmount = 10;
                panel.image = "Fairy_skill_megaheal";
                if(SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_2) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                
                break;

                //passive
                case 3:
                panel.Name = "Heal Doran";
                panel.skillCurrentLevel = player.getStat(BaseStat.StatTypes.HP).baseValue;
                panel.skillDescriptSmall = player.getStat(BaseStat.StatTypes.HP).baseValue +" "+"HP Heal";
                panel.Description = player.getStat(BaseStat.StatTypes.HP).description;
                panel.skillCost = 1;
                panel.skillPreAmount = player.getStat(BaseStat.StatTypes.HP).baseValue;
                panel.image = "Fairy_upgrade_skill";
                panel.skillNew = true;
                
                break;
                
                case 4:
                panel.Name = "Fairy Attack!";
                panel.skillCurrentLevel = player.getStat(BaseStat.StatTypes.ATK).baseValue;
                panel.skillDescriptSmall = player.getStat(BaseStat.StatTypes.ATK).baseValue +" "+"ATK DMG";
                panel.Description = player.getStat(BaseStat.StatTypes.ATK).description;
                panel.skillCost = 1;
                panel.skillPreAmount = player.getStat(BaseStat.StatTypes.ATK).baseValue;
                panel.image = "Fairy_upgrade_attack";
                panel.skillNew = true;
                
                break;
                
                case 5:
                panel.Name = "DURIAN RUNTUH";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "+5% DE";
                panel.Description = "Adds Durian drop percentage!";
                panel.skillCost = 100;
                panel.skillPreAmount = 100;
                panel.image = "Fairy_passive_durianruntuh";
                if(SkillList.getPanelPlayer(player,SkillList.FAIRY_PASSIVE_1) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                break;
                
                case 6:
                panel.Name = "LADY LUCK";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "+3% LUCK";
                panel.Description = "Increases percentage of chest encounter";
                panel.skillCost = 100;
                panel.skillPreAmount = 100;
                panel.image = "Fairy_passive_luck";
                if(SkillList.getPanelPlayer(player,SkillList.FAIRY_PASSIVE_2) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                
                break;
                
                //RESEARCH
                case 7:
                panel.Name = "Research Monster Development";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "NEW MONSTER TAB";
                panel.Description = "Some say monster can drop something to do something? hmm..";
                panel.skillCost = 0;
                panel.image = "Upgrade_learn_monster";
                if(!upgradepanel.transform.GetChild(PANEL).transform.transform.GetChild(0).GetChild(PAGE_MONSTER).gameObject.activeSelf){
                    panel.skillPreAmount = 2;
                    panel.skillNew = true;
                }
                else{
                    panel.skillPreAmount = -1;
                    panel.skillNew = false;
                }
                    
                break;
                
                case 8:
                panel.Name = "Research Shop Market";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "NEW SHOP TAB";
                panel.Description = "Interesting things can be found here they say";
                panel.skillCost = 0;
                panel.image = "Upgrade_learn_shop";
                panel.skillNew = true;
                if(!upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_SHOP).gameObject.activeSelf){
                    panel.skillPreAmount = 100000;
                    panel.skillNew = true;
                }
                else{
                    panel.skillPreAmount = -1;
                    panel.skillNew = false;
                }
                    
                break;
                
                case 9:
                panel.Name = "Research Plantation";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "NEW PLANT TAB";
                panel.Description = "Smells funny";
                panel.skillCost = 0;
                panel.image = "Upgrade_learn_planting";
                panel.skillNew = true;
                if(!upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_PLANT).gameObject.activeSelf){
                    panel.skillPreAmount = 10000000;
                    panel.skillNew = true;
                }
                else{
                    panel.skillPreAmount = -1;
                    panel.skillNew = false;
                } 
                break;
            }
            setupUI("StatPanel",panel,current_durian,i);
            
        }
        
    }
    //for doran
    public void showDoranUpgradeUI(){
        //7
        int size = 5;
        for(int i=1;i<=size;i++){
            
            BaseSkill panel = new BaseSkill();
            string panel_type = "StatPanel";
            int current_durian = player.GetComponent<DoranPlayer>().player.currentDurian;
           
            switch (i)
            {
                //doran stat list
                case 1:
                panel_type = "DoranPanel";
                setupDoranUI(panel_type,player);
                
                break;
                
                //upgrade attack
                case 2:
                panel.Name = player.getDoranStat(BaseStat.StatTypes.ATK).name;
                panel.skillCurrentLevel = player.getDoranStat(BaseStat.StatTypes.ATK).baseValue;
                panel.skillDescriptSmall = "Attacking Power";
                panel.Description = "Adds Doran ATK or CRIT!";
                panel.skillCost = 1;
                panel.skillPreAmount = player.getDoranStat(BaseStat.StatTypes.ATK).baseValue;
                panel.image = "Doran_attack";
                panel.skillNew = false;
                break;
                
                //upgrade defense
                case 3:
                panel.Name = player.getDoranStat(BaseStat.StatTypes.DEF).name;
                panel.skillCurrentLevel = player.getDoranStat(BaseStat.StatTypes.DEF).baseValue;
                panel.skillDescriptSmall = "Defensive Mode";
                panel.Description = "Adds Doran DEF or HP!";
                panel.skillCost = 1;
                panel.skillPreAmount = player.getDoranStat(BaseStat.StatTypes.DEF).baseValue;
                panel.image = "Doran_defend";
                panel.skillNew = false;
                break;
                
                case 4:
                //passive
                panel.Name = "Aura";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "Add random explosion effect";
                panel.Description = "Adds a spark in Doran's Life!";
                panel.skillCost = 1;
                panel.skillPreAmount = 10000000;
                panel.image = "Doran_attack";
                panel.skillNew = true;
                break;
                
                //skill
                case 5:
                panel.Name = "HURRICANE SLASH";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "200 DMG 1 ENEMY";
                panel.Description = "Slash and sing like a hurricane";
                panel.skillCost = 20;
                panel.skillPreAmount = 20;
                panel.image = "Doran_hurricane";
                if(SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_1) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                break;
                
                case 6:
                panel.Name = "DOPPELGANGER";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "100% ATK for 1 min";
                panel.Description = "Summon your shadow to fight for you for a moment";
                panel.skillCost = 30;
                panel.skillPreAmount = 30;
                panel.image = "Doran_dopplerganger";
                if(SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_2) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                break;
                
                case 7:
                panel.Name = "DURI MODE";
                panel.skillCurrentLevel = 0;
                panel.skillDescriptSmall = "+100% ATK, AGRO mode";
                panel.Description = "Legend foretold that a spike monster will befall for those who ate too much.";
                panel.skillCost = 40;
                panel.skillPreAmount = 40;
                panel.image = "Doran_eDurian";
                if(SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_3) == null)
                    panel.skillNew = true;
                else
                    panel.skillNew = false;
                break;
                
            }
            if(i != 1)
                setupUI(panel_type,panel,current_durian,i);
            
        }
    }
    //show monster UI
    public void showMonsterUI(){
        int size = 6;
        for(int i=0;i<size;i++){
            BaseSkill panel = new BaseSkill();
            
            int current_durian = player.GetComponent<DoranPlayer>().player.currentDurian;
            List<BaseEnemy> monsterList = player.GetComponent<DoranPlayer>().player.baseMonster;
            
            if(i>3){
                switch (i)
                {
                    
                    case 4:
                    panel.Name = "Monster Damage Up";
                    panel.skillCurrentLevel = 0;
                    panel.skillDescriptSmall = "ALL MONS DMG+";
                    panel.Description = "Increases monster damage";
                    panel.skillCost = 100;
                    panel.skillPreAmount = 100;
                    panel.image = "default";
                    panel.skillNew = true;
                    
                    break;
                    
                    case 5:
                    panel.Name = "Egg Luck Up";
                    panel.skillCurrentLevel = 0;
                    panel.skillDescriptSmall = "EGG% +";
                    panel.Description = "Increases percentage to get monster eggs";
                    panel.skillCost = 100;
                    panel.skillPreAmount = 100;
                    panel.image = "mon_egg";
                    panel.skillNew = true;
                    
                    break;
                }
                setupUI("StatPanel",panel,current_durian,i);
                
            }else{
                
                BaseEnemy monster = null;
                
                switch (i)
                {
                    case 0:
                    if(monsterList.Count > 0){
                        monster = monsterList[0];
                    }
                    break;
                    case 1:
                    if(monsterList.Count > 1){
                        monster = monsterList[1];
                    }
                    break;
                    case 2:
                    if(monsterList.Count > 2){
                        monster = monsterList[2];
                    }
                    break;
                    case 3:
                    if(monsterList.Count > 3){
                        monster = monsterList[3];
                    }
                    break;
                }
                
                if(monster != null){
                    
                    panel.Name = monster.Name;
                    panel.skillCurrentLevel = monster.Level;
                    panel.skillDescriptSmall = "Level "+monster.Level;
                    panel.Description = "MODE: "+ monster.monsterMode;
                    panel.skillCost = 0;
                    panel.skillPreAmount = 100;
                    panel.skillNew = false;
                    if(monster.monsterType == BaseEnemy.MonsterTypes.EGG){
                        panel.image = "mon_egg";
                        setupUI("StatPanel",panel,current_durian,i);
                    }else{
                        panel.image = "mon_toge";
                        Sprite sprite = ImageManager.getSpriteFromResources(panel.image);
                        setupMonsterUI(panel.Name,panel.skillCurrentLevel,panel.skillDescription,sprite,0);
                    }
                }else{
                    panel.Name = "No EGG";
                    panel.skillCurrentLevel = 0;
                    panel.skillDescriptSmall = "";
                    panel.Description = "";
                    panel.skillCost = 0;
                    panel.skillPreAmount = -1;
                    panel.image = "mon_egg3";
                    panel.skillNew = false;
                    setupUI("StatPanel",panel,current_durian,i);
                }
                
                }
        }
    }
    string filterDurian(int req_durian){
        string durian = ""+ req_durian;
            int min = 1000, max = 1000000;
            if(req_durian >= min && req_durian < max){
                req_durian /= min;
                durian = req_durian + "k";
            }
            min = 1000000;
            max = 1000000000;
            if(req_durian >= min && req_durian < max){
                req_durian /= min;
                durian = req_durian + "m";
            }
            return durian;
    }
    void setupUI(string panel_type,BaseSkill panel,int current_durian,int current_num){
        string title = panel.Name;
        string current = panel.skillDescriptSmall;
        string description = panel.skillDescription;
        Sprite sprite = ImageManager.getSpriteFromResources(panel.image);
        int req_durian = panel.skillPreAmount;
        string dmg_type = "Level Up";
        int dmg = panel.skillCost;
        switch (CURRENT_PAGE)
        {
            //DORAN PAGE
            case 0:
            
            if(current_num == 2){
                GameObject text_active = Instantiate(Resources.Load<GameObject>("UI/ContentText")) as GameObject;
                text_active.transform.SetParent(upgradewindow.transform,false);
                text_active.transform.GetChild(0).GetComponent<Text>().text = "PASSIVE";
                text_active.tag = "List";
            }else if(current_num == 5){
                GameObject text_active = Instantiate(Resources.Load<GameObject>("UI/ContentText")) as GameObject;
                text_active.transform.SetParent(upgradewindow.transform,false);
                text_active.transform.GetChild(0).GetComponent<Text>().text = "ACTIVE";
                text_active.tag = "List";
            }
            
            break;
            //FAIRY PAGE
            case 1:
            if(current_num == 1){
                GameObject text_active = Instantiate(Resources.Load<GameObject>("UI/ContentText")) as GameObject;
                text_active.transform.SetParent(upgradewindow.transform,false);
                text_active.transform.GetChild(0).GetComponent<Text>().text = "ACTIVE";
                text_active.tag = "List";
            }else if(current_num == 5){
                GameObject text_active = Instantiate(Resources.Load<GameObject>("UI/ContentText")) as GameObject;
                text_active.transform.SetParent(upgradewindow.transform,false);
                text_active.transform.GetChild(0).GetComponent<Text>().text = "PASSIVE";
                text_active.tag = "List";
            }
            else if(current_num == 7){
                GameObject text_active = Instantiate(Resources.Load<GameObject>("UI/ContentText")) as GameObject;
                text_active.transform.SetParent(upgradewindow.transform,false);
                text_active.transform.GetChild(0).GetComponent<Text>().text = "RESEARCH";
                text_active.tag = "List";
            }
            break;
            
        }
        
        GameObject statPanel = Instantiate(Resources.Load<GameObject>("Panel/"+panel_type)) as GameObject;
        statPanel.name = title;
        if(panel.skillNew){
            //statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().material.shader = Resources.Load<Shader>("Shader/Grayscale");
            statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.grey;
        }
        else{
            //statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().material.shader = null;
            statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.white;
        }
           
        
        statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = sprite;
        statPanel.transform.GetChild(1).GetComponent<Text>().text = title;
        statPanel.transform.GetChild(2).GetComponent<Text>().text = current;
        statPanel.transform.GetChild(4).GetComponent<Text>().text = description;
        statPanel.tag = "List";
        
        Button button_panel = statPanel.transform.GetChild(3).GetComponent<Button>();
        button_panel.onClick.AddListener(delegate { upgradeClick(statPanel,current_num,CURRENT_PAGE,req_durian,dmg); });
        string text = "";
        if(dmg == 0)
            text = dmg_type;
        else
            text = dmg_type + "+" + dmg;
        button_panel.transform.GetChild(0).GetComponent<Text>().text = text;
        button_panel.transform.GetChild(1).GetComponent<Text>().text = filterDurian(req_durian);
        statPanel.transform.SetParent(upgradewindow.transform,false);
        
        if(current_durian < req_durian){
            
            button_panel.interactable = false;
            statPanel.transform.GetChild(3).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("sub_menu_grey");
            //statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.grey;
        }
        else{
            if(req_durian < 0){
                button_panel.gameObject.SetActive(false);
                statPanel.transform.GetChild(3).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("sub_menu_grey");
                //statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.grey;
            
            }else{
                button_panel.interactable = true;
                statPanel.transform.GetChild(3).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("sub_menu");
                statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().color = Color.white;
            }
        }
    }
    void setupMonsterUI(string name,int level,string description,Sprite image,int condition){
        GameObject monpanel = Instantiate(Resources.Load<GameObject>("Panel/MonsterPanel")) as GameObject;
        monpanel.name = "MonPanel" + name;
        monpanel.tag = "List";
        monpanel.transform.GetChild(0).GetChild(0).GetComponent<Image>().sprite = image;
        monpanel.transform.GetChild(1).GetComponent<Text>().text = name;
        switch (condition)
        {
            case 0:
            monpanel.transform.GetChild(2).GetComponent<Image>().color = Color.green;
            monpanel.transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("mon_happy");
            break;
            case 1:
            monpanel.transform.GetChild(2).GetComponent<Image>().color = Color.yellow;
            monpanel.transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("mon_tired");
            break;
            case 2:
            monpanel.transform.GetChild(2).GetComponent<Image>().color = Color.red;
            monpanel.transform.GetChild(2).GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources("mon_sick");
            break;
        }
        monpanel.transform.GetChild(3).GetComponent<Text>().text = "Level "+level;
        monpanel.transform.GetChild(5).GetComponent<Text>().text = description;
        //TODO: setup click to remove monster
        
        monpanel.transform.SetParent(upgradewindow.transform,false);
        
        //TODO: set click to remove monster
    }
    void upgradeClick(GameObject obj,int num,int type,int amount,int dmg){
        //type is current page type
        //num is row number
        
        //show particle
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Spark")) as GameObject;
        //skillparticle.transform.localPosition = Camera.main.ScreenToWorldPoint(GameObject.FindGameObjectWithTag("Notification").transform.position);
        skillparticle.transform.SetParent(obj.transform, false);
        
        Destroy(skillparticle,2f);
            
        //int durian = player.new_player.currentDurian;
        //set requirement for upgrade
        switch (type)
        {
            case 0: //for doran
            switch (num)
            {
                //ATK
                case 2:
                //Debug.Log("add atk");
                player.addDoranStat(BaseStat.StatTypes.ATK,dmg);
                break;
                
                //DEF
                case 3:
                player.addDoranStat(BaseStat.StatTypes.DEF,dmg);
                break;
                
                //AURA
                case 4:
                
                break;
                
                //SKILL 1
                case 5:
                /*
                //skill 1, hurricane
                BaseSkill sk = SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_1);
                if(sk == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.DORAN_SKILL_1);
                    player.new_player.baseSkill.Add(skill);
                    var index =  player.new_player.baseSkill.FindIndex(a => a.skillName == skill.Name);
                    addSkilltoPanel(index,skill);
                }else
                {
                    Debug.Log("add skill level here");
                }
                */
                break;
                
                //SKILL 2
                case 6:
                //skill 2, another me
                BaseSkill sk2 = SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_2);
                if(sk2 == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.DORAN_SKILL_2);
                    player.player.baseSkill.Add(skill);
                    var index =  player.player.baseSkill.FindIndex(a => a.skillName == skill.Name);
                    addSkilltoPanel(index,skill);
                }else
                {
                    Debug.Log("add skill level here");
                }
                break;
                
                //SKILL 3
                case 7:
                //skill 3, durian mode
                BaseSkill sk3 = SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_3);
                if(sk3 == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.DORAN_SKILL_3);
                    player.player.baseSkill.Add(skill);
                    var index =  player.player.baseSkill.FindIndex(a => a.skillName == skill.Name);
                    addSkilltoPanel(index,skill);
                }else
                {
                    Debug.Log("add skill level here");
                }
                break;
                
                default:
                break;
            }
            //remove and show update
            removeContent(PAGE_DORAN);
            break;
            
            case 1: //for player
            switch (num)
            {
                case 5:
                player.addBaseStat(BaseStat.StatTypes.HP,dmg);
                break;
                case 6:
                player.addBaseStat(BaseStat.StatTypes.ATK,dmg);
                break;
                
                case 3:
                //durian percentage
                BaseSkill ps = SkillList.getSkillPlayer(player,SkillList.FAIRY_PASSIVE_1);
                if(ps == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.FAIRY_PASSIVE_1);
                    player.player.basePanel.Add(skill);
                }else
                {
                    Debug.Log("add passive level here");
                }
                break;
                
                case 4:
                //chest percentage
                BaseSkill ps2 = SkillList.getSkillPlayer(player,SkillList.FAIRY_PASSIVE_2);
                if(ps2 == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.FAIRY_PASSIVE_2);
                    player.player.basePanel.Add(skill);
                }else
                {
                    Debug.Log("add passive level here");
                }
                break;
                
                case 1:
                //skill 1, dark harvested
                BaseSkill sk = SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_1);
                if(sk == null){
                    BaseSkill skill = SkillList.getSkillInfo(SkillList.FAIRY_SKILL_1);
                    player.player.baseSkill.Add(skill);
                    var index =  player.player.baseSkill.FindIndex(a => a.skillName == skill.Name);
                    addSkilltoPanel(index,skill);
                }else
                {
                    Debug.Log("add skill level here");
                }
                
                
                break;
                
                case 2:
                //skill 2, mega heal
                BaseSkill sk2 = SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_2);
                if(sk2 == null){
                    BaseSkill skill2 = SkillList.getSkillInfo(SkillList.FAIRY_SKILL_2);
                    player.player.baseSkill.Add(skill2);
                    var index =  player.player.baseSkill.FindIndex(a => a.skillName == skill2.Name);
                    addSkilltoPanel(index,skill2);
                }else
                {
                    Debug.Log("add skill level here");
                }
                break;
                
                case 7:
                //research mons
                upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_MONSTER).gameObject.SetActive(true);
                break;
                
                case 8:
                //research shop
                upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_SHOP).gameObject.SetActive(true);
                break;
                
                case 9:
                //research plantation
                upgradepanel.transform.GetChild(PANEL).transform.GetChild(0).GetChild(PAGE_PLANT).gameObject.SetActive(true);
                break;
                
                default:
                break;
            }
            //remove and show update
            removeContent(PAGE_FAIRY);
            break;
            
            case 2: //for monster
            switch (num)
            {
                //monster/egg 1
                case 0:
                player.changeEggtoMon(0);
                break;
                
                case 1:
                player.changeEggtoMon(1);
                break;
                
                case 2:
                player.changeEggtoMon(2);
                break;
                
                case 3:
                player.changeEggtoMon(3);
                break;
                
                case 4:
                break;
                
                default:
                break;
            }
            //remove and show update
            removeContent(PAGE_MONSTER);
            break;
            
            default:
            break;
        }
        player.minusDurian(amount);
        
    }
    void setupDoranUI(string panel_type,DoranPlayer player){
        BasePlayer panel = player.player;
        GameObject statPanel = Instantiate(Resources.Load<GameObject>("Panel/"+panel_type)) as GameObject;
        statPanel.name = panel.Name;
        Sprite sprite = ImageManager.getSpriteFromResources(panel.image);
        statPanel.transform.GetChild(0).transform.GetChild(0).GetComponent<Image>().sprite = sprite;
        statPanel.transform.GetChild(1).GetComponent<Text>().text = panel.Name;
        statPanel.transform.GetChild(2).GetComponent<Text>().text = "Level:" + panel.level;
        statPanel.transform.GetChild(3).GetComponent<Text>().text = "DPS to Damage:-";
        statPanel.transform.GetChild(4).GetComponent<Text>().text = "Average DPS:-";
        statPanel.transform.GetChild(5).GetComponent<Text>().text = player.getDoranStat(BaseStat.StatTypes.ATK).name +": "+
        player.getDoranStat(BaseStat.StatTypes.ATK).baseValue;
        statPanel.transform.GetChild(6).GetComponent<Text>().text = player.getDoranStat(BaseStat.StatTypes.CRIT).name +": "+
        player.getDoranStat(BaseStat.StatTypes.CRIT).baseValue+"%";
        statPanel.transform.GetChild(7).GetComponent<Text>().text = player.getDoranStat(BaseStat.StatTypes.DEF).name +": "+
        player.getDoranStat(BaseStat.StatTypes.DEF).baseValue;
        statPanel.transform.GetChild(8).GetComponent<Text>().text = player.getDoranStat(BaseStat.StatTypes.HP).name +": "+
        player.getDoranStat(BaseStat.StatTypes.HP).baseValue;
        statPanel.tag = "List";
        statPanel.transform.SetParent(upgradewindow.transform,false);
    }
    //skill button(at front UI)
    void addSkilltoPanel(int panel_num,BaseSkill skill){
        
        GameObject button = maincanvas.transform.GetChild(SKILL).GetChild(panel_num).gameObject;
        //set image and click
        button.transform.GetChild(0).GetComponent<Image>().sprite = ImageManager.getSpriteFromResources(skill.image);
        button.transform.GetChild(0).GetComponent<Image>().enabled = true;
        button.GetComponent<Button>().onClick.AddListener(delegate { skillButton(panel_num,skill.Name,skill.SkillCooldown); });
    }
    public void skillButton(int panel_num,string name,int cooldown){
        GameObject button = maincanvas.transform.GetChild(SKILL).GetChild(panel_num).gameObject;
        button.GetComponent<Button>().interactable = false;
        button.transform.GetChild(1).gameObject.SetActive(true);
        StartCoroutine (Countdown(cooldown,button));
        //attack skill
        GameObject fairy = GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).gameObject;
        switch (name)
        {
            case "THE DARK HARVEST":
            fairy.GetComponent<FingerMove>().fairySkill(0,SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_1));
            break;
            
            case "MEGA HEAL":
            fairy.GetComponent<FingerMove>().fairySkill(1,SkillList.getSkillPlayer(player,SkillList.FAIRY_SKILL_2));
            break;
            
            case "HURRICANE SLASH":
            fairy.GetComponent<FingerMove>().doranSkill(0,SkillList.getSkillPlayer(player,SkillList.DORAN_SKILL_1));
            break;
            
            case "DOPPELGANGER":
            GameObject dopple = Instantiate(Resources.Load<GameObject>("Prefabs/Dopple")) as GameObject;
            dopple.transform.SetParent(player.transform,false);
            Destroy(dopple,10);
            break;
            
            case "DURI MODE":
            player.transform.GetChild(0).GetComponent<PlayerAttack>().duriMode(true);
            
            break;
            
            default:
            break;
        }
        
    }
    
    private IEnumerator Countdown(int time,GameObject button){
        while(time>0){
            
            string text = "";
            /* not finished yet
            if(time > 60){
                int minute = time / 60;
                int extra = time - (minute * 60);
                text = minute + ":" + extra;
            }
            if(time < 10)
                text = "00:0";
            */
            time--;
            button.transform.GetChild(1).GetComponent<Text>().text = text+time;
            yield return new WaitForSeconds(1);
        }
        button.GetComponent<Button>().interactable = true;
        button.transform.GetChild(1).gameObject.SetActive(false);
    }
     public void showPanelText(string text){
        if(!TEXT_PANEL){
            if(upgradepanel.transform.childCount < 8){
                TEXT_PANEL = true;
                GameObject textpanel = Instantiate(Resources.Load<GameObject>("Prefabs/TextPanel")) as GameObject;
                textpanel.transform.SetParent(upgradepanel.transform, false);
                textpanel.transform.GetChild(0).GetComponent<Text>().text = text;
                StartCoroutine(HideTextPanel()); 
                }else{
                    upgradepanel.transform.GetChild(7).GetComponent<Animator>().SetTrigger("hide");
                    Destroy(upgradepanel.transform.GetChild(7).gameObject,1);
                    TEXT_PANEL = false;
           }
        }
        
    }
    IEnumerator HideTextPanel() {
        yield return new WaitForSeconds(2);
        upgradepanel.transform.GetChild(7).GetComponent<Animator>().SetTrigger("hide");
        Destroy(upgradepanel.transform.GetChild(7).gameObject,1);
        TEXT_PANEL = false;
    }

    public void updateElemText(BasePlayer player){
        fire_amount.text = "" + player.currentFire;
        ice_amount.text = "" + player.currentIce;
        thunder_amount.text = "" + player.currentThunder;
        leaf_amount.text = "" + player.currentLeaf;
    }
    public void updateBuahText(BasePlayer player){
        amount_cili.text = "" + player.amt_cili;
        amount_nangka.text = "" + player.amt_nangka;
        amount_kelapa.text = "" + player.amt_kelapa;
        amount_bawang.text = "" + player.amt_bawang;
    }
    public void updateElemStatus(DoranPlayer player){
        if(player.checkPlayerHasFruit(StopPoint.StopTypes.FIRE))
            img_cili.enabled = true;
        else
            img_cili.enabled = false;
        if(player.checkPlayerHasFruit(StopPoint.StopTypes.ICE))
            img_kelapa.enabled = true;
        else
            img_kelapa.enabled = false;
        if(player.checkPlayerHasFruit(StopPoint.StopTypes.LEAF))
            img_nangka.enabled = true;
        else
            img_nangka.enabled = false;
        if(player.checkPlayerHasFruit(StopPoint.StopTypes.THUNDER))
            img_bawang.enabled = true;
        else
            img_bawang.enabled = false;
    }
    
}
