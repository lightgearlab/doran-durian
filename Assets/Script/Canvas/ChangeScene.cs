﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour {

    public static int SCENE_TITLE = 0;
    public static int SCENE_BATTLE = 1;
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public  void changeScene(int scene){
        switch (scene)
        {
            case 0:
            
            break;
            
            case 1:
            break;
            
            default:
            break;
        }
        SceneManager.LoadScene(scene);
    }
    public static void playTutorial(){
        GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>().startTutorial();
    }
    //repeat battle stage
    public static void playAnimation(){
        
        GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<Animator>().SetTrigger("scene_change");
    }
    public void playAnimationButton(){
        GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<Animator>().SetTrigger("scene_change");
    }
    public void changeSceneBattle(){
        //GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>().saveCurrentPlayerLocation();
        GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>().setWhichPlayerBattling();
        GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<Animator>().SetTrigger("battle_mode");
    }
    public static void startScene(){
        GameObject.FindGameObjectWithTag("ChangeScene").GetComponent<Animator>().SetTrigger("start_stage");
    }
    public void BattleScene(){
        SceneManager.LoadScene ("BattleScene", LoadSceneMode.Single);
    }
}
