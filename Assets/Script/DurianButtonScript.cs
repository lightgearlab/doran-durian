﻿using UnityEngine;
using System.Collections;
using System;
public class DurianButtonScript : MonoBehaviour {

	GameObject battleMode,player;
    BattleManager battleManager;
	// Use this for initialization
	void Start () {
		battleMode = GameObject.FindGameObjectWithTag("MainCanvas").transform.GetChild(CanvasScript.BATTLE_MODE).gameObject;
		player = GameObject.FindGameObjectWithTag("Player");
        battleManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void remove1DurianFromBucket(){
        GetComponent<Animator>().SetTrigger("removedurian");
        StartCoroutine(battleManager.endTurn());
		StartCoroutine(ShiftLeft());
        
    }
	public IEnumerator ShiftLeft() {
        int num = Int32.Parse(transform.parent.name);
        Debug.Log("shift "+num);
        float delay = 1;
        yield return new WaitForSeconds(delay);
        
        Transform bucket = battleMode.transform.GetChild(3).GetChild(1);
        if(num != battleManager.getBucket().Count){

            for(int i=num;i<5;i++){
                if(bucket.GetChild(i).childCount > 0){ //if there is something, shift left
                    Transform durian = bucket.GetChild(i).GetChild(0);
                    durian.SetParent(bucket.GetChild(i-1));
                    durian.position = bucket.GetChild(i-1).position;
                }
            }
            
        }
        
        //StartCoroutine(player.GetComponent<DoranState>().endTurn());
        battleManager.removeDurianFromBucketNum(num-1);
        Destroy(gameObject,1);

    }
}
