﻿using UnityEngine;
using System.Collections;

public class FootStepsSound : MonoBehaviour {

	AudioSource audioSource;
	public AudioClip[] audioClip;
	// Use this for initialization
	void Start () {
		audioSource = GetComponent<AudioSource> ();
	}

	public void LeftSound()
	{
		int random = Random.Range (0, 4);
		audioSource.clip = audioClip [random];
		audioSource.Play ();
	}

	public void RightSound()
	{
		int random = Random.Range (0, 4);
		audioSource.clip = audioClip [random];
		audioSource.Play ();
	}
	

}
