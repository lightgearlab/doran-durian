﻿using UnityEngine;
using System.Collections;

public class MoveToPlayer : MonoBehaviour {

	PlayerHealth playerHealth;

	public float speed;
	public float absorbRange;

	void Update () 
	{
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        if(player != null){
            if (!checkInRange(player,absorbRange)) 
            {
                Absorb(player);
            }
        }
		
        
	}

	bool checkInRange(GameObject player,float chaseRange)
	{
		float absorbDistance = Vector3.Distance (transform.position, player.transform.position);

		if (absorbDistance < absorbRange)
			return false;
		else 
			return true;
		
	}
	public void Absorb(GameObject player)
	{
        float step = speed * Time.deltaTime;
        gameObject.transform.position = Vector3.MoveTowards(transform.position, player.transform.position, step);
    
	}
}
