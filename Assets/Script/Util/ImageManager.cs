﻿using UnityEngine;

public class ImageManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	public static Sprite getSpriteFromSpriteSheet(string path,string imageName){
			Sprite[] sprites = Resources.LoadAll<Sprite>(path);
            foreach (Sprite sprite in sprites)
            {
                if (sprite.name == imageName)
					return sprite;
            }
			Sprite notfound = Resources.Load<Sprite>("UI/gamegui");
			//return null;
			return notfound;
	}
	public static Sprite getSpriteFromResources(string imageName){
			
			Sprite sprite = Resources.Load<Sprite>("UI/"+imageName);
			if(sprite != null)
				return sprite;
				else{
					return Resources.Load<Sprite>("UI/"+"default");
				}
			
	}
	// Update is called once per frame
	void Update () {
	
	}
}
