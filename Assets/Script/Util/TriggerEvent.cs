﻿using UnityEngine;
using System.Collections;

public class TriggerEvent : MonoBehaviour {

    public void triggerLevel(){
        GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>().triggerLevel();
    }
    public void setColorGrey(){
        GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>().setColorGrey();
    }
    public void dimLevelList(){
        GameObject depthManager = GameObject.FindGameObjectWithTag("Depth");
        depthManager.GetComponent<Animator>().SetTrigger("depth_dim");
    }
    public void dimGroupList(){
        GameObject depthManager = GameObject.FindGameObjectWithTag("Depth");
        GameObject group = depthManager.transform.GetChild(3).gameObject;
        for(int i=0;i<group.transform.childCount;i++){
            //group.transform.GetChild(i).GetComponent<Animator>().SetTrigger("dim");
        }
    }
    /*
    void OnTriggerEnter(Collider other) {
        //trigger when doran enters
        Debug.Log("trigger");
       if(other.tag == "Player"){
            GameObject player = GameObject.FindGameObjectWithTag("Player");
           switch (state)
           {
               case DoranState.State.END:
               
               
               //endless loop
               player.transform.position = startPoint.position;
               //update next module
               SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
               spawnManager.spawnModule();
               
               
               break;
               
               case DoranState.State.GODOWN:
               
               break;
               
               default:
               //reset camera
               player.transform.rotation = Quaternion.Euler(0,0,0);
               //update states
               player.GetComponent<DoranState>().currentState = state;
               break;
           }
           
           
           
       }
    }
    */
}
