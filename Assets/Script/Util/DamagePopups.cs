﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DamagePopups : MonoBehaviour 
{
	public GameObject damagePrefab;


	public void CreateDamagePopup(int damage)
	{
		int randomRange= Random.Range (-10, 10); // choose random number from -10 to 10

		//apply randomPosition using randomRange
		Vector3 randomPosition = new Vector3 (damagePrefab.transform.position.x + randomRange,0, damagePrefab.transform.position.z + randomRange);

		//Instantiate DamageText randomly
		GameObject damageGameObject = (GameObject)Instantiate(damagePrefab,
		                                                      randomPosition,
		                                                      damagePrefab.transform.rotation);
		damageGameObject.GetComponentInChildren<Text>().text = damage.ToString(); // what to show in Text
		damageGameObject.transform.SetParent(this.gameObject.transform,false); // set parent for damagePopUps
        Destroy(damageGameObject,2f);
	}
    public void CreateHealPopup(int amount)
	{
		int randomRange= Random.Range (-10, 10); // choose random number from -10 to 10

		//apply randomPosition using randomRange
		Vector3 randomPosition = new Vector3 (damagePrefab.transform.position.x + randomRange, 0, damagePrefab.transform.position.z + randomRange);

		//Instantiate DamageText randomly
		GameObject damageGameObject = (GameObject)Instantiate(damagePrefab,
		                                                      randomPosition,
		                                                      damagePrefab.transform.rotation);
        damageGameObject.GetComponent<Text>().color = Color.green;
        damageGameObject.GetComponent<Text>().fontSize = 10;
		damageGameObject.GetComponentInChildren<Text>().text = amount.ToString(); // what to show in Text
		damageGameObject.transform.SetParent(this.gameObject.transform,false); // set parent for damagePopUps
        Destroy(damageGameObject,2f);
	}
}
