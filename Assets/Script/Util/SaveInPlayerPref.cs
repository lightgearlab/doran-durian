﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveInPlayerPref  {

	public static void saveCurrentPlayer(GameObject player){
		PlayerPrefs.SetString("currentPlayer",player.name);
	}
	public static void saveCurrentPlayerLocation(List<GameObject> playerList){
		string playersName = "";
		for(int i=0;i<playerList.Count;i++){
			GameObject player = playerList[i];
			PlayerPrefs.SetString(player.name,player.transform.position.x+";"+
			player.transform.position.y+";"+
			player.transform.position.z);

			if(i!=playerList.Count-1)
				playersName += player.name + ";";
			else
				playersName += player.name;
		}
		PlayerPrefs.SetString("playerTurn",playersName);
		//Debug.Log("---"+playersName);
		PlayerPrefs.SetInt("amount",playerList.Count);
	}
}
