﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TutorialManagerScript : MonoBehaviour {

	public static int TUTORIAL_NUM = 0;
	GameObject anim_obj,fairy,main_canvas,upgrade_panel,skill,player,notifi;
	// Use this for initialization
	void Awake () {
		anim_obj = GameObject.FindGameObjectWithTag("Durian");
		fairy = GameObject.FindGameObjectWithTag("Fairy");
		main_canvas = GameObject.FindGameObjectWithTag("MainCanvas");
		upgrade_panel = GameObject.FindGameObjectWithTag("UpgradePanel");
		player = GameObject.FindGameObjectWithTag("Player");
		skill = main_canvas.transform.GetChild(3).gameObject;
		//notifi = GameObject.FindGameObjectWithTag("Notification");

	}
	public void executeTutorial(){
		switch (TUTORIAL_NUM)
		{
			//learn to tap screen
			case 0:
			//GameObject tutorial = Instantiate(Resources.Load<GameObject>("Tutorial/Tutorial_hand")) as GameObject;
			//tutorial.transform.SetParent(notifi.transform,false);
			fairy.SetActive(false);
			main_canvas.transform.GetChild(2).gameObject.SetActive(false);
			upgrade_panel.SetActive(false);
			skill.SetActive(false);
			
			break;
			
			//unlock fairy, learn to heal or attack
			case 1:
			Destroy(GameObject.FindGameObjectWithTag("Hand"));
			GameObject fairy_tutorial = Instantiate(Resources.Load<GameObject>("Tutorial/FairyTutorial")) as GameObject;
			fairy_tutorial.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = ""+
			"Hey!\nCan i join you?\nI can attack and heal you!\n\nTap to proceed";
			fairy_tutorial.transform.SetParent(anim_obj.transform,false);
			fairy_tutorial.GetComponent<Button>().onClick.AddListener(delegate { spawnNext(fairy_tutorial,0); });
			break;
			
			//unlock bottom tab, learn to upgrade , use skill
			case 2:
			GameObject fairy_tutorial2 = Instantiate(Resources.Load<GameObject>("Tutorial/FairyTutorial")) as GameObject;
			fairy_tutorial2.transform.GetChild(0).GetChild(0).GetComponent<Text>().text = ""+
			"Hey!\nDid you know you can do awesome skills?\nUpgrade and you'll see!\n\nTap to proceed";
			fairy_tutorial2.transform.SetParent(anim_obj.transform,false);
			fairy_tutorial2.GetComponent<Button>().onClick.AddListener(delegate { spawnNext(fairy_tutorial2,1); });
			break;
			
			//learn to unlock monster, get egg and hatch egg
			case 3:
			GameObject tutorial3 = Instantiate(Resources.Load<GameObject>("Tutorial/EggDialog")) as GameObject;
			tutorial3.transform.SetParent(anim_obj.transform,false);
			
			//add egg
			BaseEnemy enemy = new BaseEnemy();
			enemy.Name = "Toge";
			enemy.MonsterMode = BaseEnemy.MonsterModes.ATTACK;
			enemy.monsterLevel = 1;
			enemy.monsterType = BaseEnemy.MonsterTypes.EGG;
			player.GetComponent<DoranPlayer>().player.baseMonster.Add(enemy);
			
			tutorial3.GetComponent<Button>().onClick.AddListener(delegate { spawnNext(tutorial3,2); });
			break;
			
		}
		TUTORIAL_NUM++;
	}
	public void noTutorial(){
		
		fairy.SetActive(true);
		main_canvas.transform.GetChild(2).gameObject.SetActive(true);
		upgrade_panel.SetActive(true);
		
		player.GetComponent<PlayerHealth>().initialize();
		player.GetComponent<PlayerHealth>().initializeHP();
		main_canvas.GetComponent<CanvasScript>().initialze();
		main_canvas.GetComponent<CanvasScript>().removeContent(0);

		//disable skill
		//skill.SetActive(true);
		//notifi.GetComponent<NotificationScript>().showRandomNotify();
		//notifi.GetComponent<NotificationScript>().showRandom(true);
	}
	public void spawnNext(GameObject obj,int num){
		
		Destroy(obj);
		switch (num)
		{
			case 0:
			fairy.SetActive(true);
			main_canvas.transform.GetChild(2).gameObject.SetActive(true);
			upgrade_panel.SetActive(true);
			
			player.GetComponent<PlayerHealth>().initialize();
			player.GetComponent<PlayerHealth>().initializeHP();
			main_canvas.GetComponent<CanvasScript>().initialze();
			main_canvas.GetComponent<CanvasScript>().removeContent(0);
			break;
			
			case 1:
			skill.SetActive(true);
			//notifi.GetComponent<NotificationScript>().showRandomNotify();
			//notifi.GetComponent<NotificationScript>().showRandom(true);
			//GameObject.FindGameObjectWithTag("MainCamera").transform.GetChild(0).GetComponent<FingerMove>().changeFairyMode();
			break;
			
			case 2:
			//last tutorial
			SpawnManager.TUTORIAL = false;
			break;
		}
         //spawn
        SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
        spawnManager.spawnModule();
    }
}
