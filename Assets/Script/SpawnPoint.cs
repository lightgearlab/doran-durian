﻿using UnityEngine;
using System.Collections.Generic;

public class SpawnPoint : MonoBehaviour {

	public Transform cameraPos;
	public bool IsContainMonster;
	GameObject mainCamera;
	public int spawnPointNumber;
	public Transform left,right,down,up;

	void Start(){
		mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
	}
	 void OnTriggerEnter(Collider other) {
       if(other.tag == "Player"){
		   other.GetComponent<DoranState>().atSpawnPoint(spawnPointNumber);
		   other.GetComponent<DoranState>().move_left = left;
		   other.GetComponent<DoranState>().move_right = right;
		   other.GetComponent<DoranState>().move_down = down;
		   other.GetComponent<DoranState>().move_up = up;

		   other.GetComponent<DoranState>().changeStateAndExecuteTurn(DoranState.State.IDLE);
		   if(IsContainMonster){
			   //set battle mode
			   other.GetComponent<DoranState>().changeStateAndExecuteTurn(DoranState.State.BATTLE);
		   }
		   //change camera
		   mainCamera.transform.localPosition = cameraPos.position;
           mainCamera.transform.rotation = cameraPos.rotation;

		   //TODO: SPAWN somthing else too
		   //spawn monster
		   //spawnMonsterManager();
		}
	}
	void OnTriggerStay(Collider other) {
       if(other.tag == "Enemy"){
		    IsContainMonster = true;
		}else{
			IsContainMonster = false;
		}
	}
	void spawnMonsterManager(){
		GameObject monster = Instantiate(Resources.Load("Prefabs/Manager/MonsterSpawnManager")) as GameObject;
		monster.transform.position = transform.position;
		monster.transform.SetParent(transform);
	}
	
}
