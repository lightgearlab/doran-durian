﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
public class DoranPlayer : MonoBehaviour {

    //setup dummy data
    int current_durian = 0;
    
	Text durian_amount;
    public BasePlayer player;
    public BaseDoran doranStat;
    GameObject mainCanvas,upgradePanel;
    int monsterTarget = 10;
    int monsterTotalTarget = 10;
    public FloatReference maxHP;

	void Start () {
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        upgradePanel = GameObject.FindGameObjectWithTag("UpgradePanel");
        if(mainCanvas!=null){
            durian_amount = mainCanvas.transform.GetChild(0).transform.GetChild(0).GetComponent<Text>();
            //default
            updateProgressBar(0);
        }
        //initialize durian
	    
        //new player or continue from database
        //temporary
        player = new BasePlayer();
        doranStat = new BaseDoran();
        
        //create new player
        createNewPlayer(player);
        createNewDoran(doranStat);

        

        
	}
    void createNewPlayer(BasePlayer new_player){
        new_player.name = "Doran";
        new_player.currentDurian = 0;
        new_player.currentStage = 0;
        //new_player.currentWeapon = 0;
        new_player.currentFire = 0;
        new_player.currentIce = 0;
        new_player.currentLeaf = 0;
        new_player.currentThunder = 0;
        new_player.amt_cili = 0;
        new_player.amt_kelapa = 0;
        new_player.amt_nangka = 0;
        new_player.amt_bawang = 0;
        new_player.currentTurn = 1;
        new_player.has_cili = false;
        new_player.has_bawang = false;
        new_player.has_kelapa = false;
        new_player.has_nangka = false;
        new_player.ownedSpots = new List<StopPoint>();
        new_player.durianPercentage = 1.0f;
        new_player.monsterKilled = new List<int>();
        new_player.image = "Doran_dp";
        
        //setup basic stat
        // List<BaseStat> classStat = new List<BaseStat>();
        // BaseHP HP = new BaseHP();
        // BaseATK ATK = new BaseATK();
        // HP.StatBaseValue = 10;
        // ATK.StatBaseValue = 10;
        // classStat.Add(HP);
        // classStat.Add(ATK);
        
        // new_player.baseStat = classStat;
        
        //add one egg
        //BaseEnemy enemy = new BaseEnemy();
        //enemy.Name = "Toge";
        //enemy.MonsterMode = BaseEnemy.MonsterModes.ATTACK;
        //enemy.monsterLevel = 1;
        //enemy.monsterType = BaseEnemy.MonsterTypes.EGG;
        
        //List<BaseEnemy> monsterList = new List<BaseEnemy>();
        //monsterList.Add(enemy);
        
        //new_player.baseMonster = monsterList;
        
        //empty skill
        //List<BaseSkill> skillList = new List<BaseSkill>();
        //new_player.baseSkill = skillList;
        
        //empty panel
        //List<BaseSkill> panelList = new List<BaseSkill>();
        //new_player.basePanel = panelList;

    }
    void createNewDoran(BaseDoran doran){
        doran.currentWeapon = 0;
        doran.baseStat = initializeStat();
    }
    public List<BaseStat> initializeStat()
    {
        List<BaseStat> classStat = new List<BaseStat>();
        BaseATK ATK = new BaseATK();
        BaseCRIT CRIT = new BaseCRIT();
        BaseDEF DEF = new BaseDEF();
        BaseHP HP = new BaseHP();
        
        ATK.StatBaseValue = 10;
        CRIT.StatBaseValue = 1;
        DEF.StatBaseValue = 1;
        HP.StatBaseValue = 100;
        
        classStat.Add(ATK);
        classStat.Add(CRIT);
        classStat.Add(DEF);
        classStat.Add(HP);

        return classStat;
    }
    public BaseStat getStat(BaseStat.StatTypes stat){
         switch (stat)
        {
            case BaseStat.StatTypes.HP:
            return player.baseStat[0];
            
            case BaseStat.StatTypes.ATK:
            return player.baseStat[1];
            
            default:
            Debug.Log("Error Player Stat Log");
            return null;
        }
    }
    public BaseStat getDoranStat(BaseStat.StatTypes stat){
         switch (stat)
        {
            case BaseStat.StatTypes.ATK:
            return doranStat.baseStat[0];
            
            case BaseStat.StatTypes.CRIT:
            return doranStat.baseStat[1];
            
            case BaseStat.StatTypes.DEF:
            return doranStat.baseStat[2];
            
            case BaseStat.StatTypes.HP:
            return doranStat.baseStat[3];
            
            default:
            Debug.Log("Error Doran Stat Log");
            return null;
        }
    }
    public void addBaseStat(BaseStat.StatTypes stat,int amount){
        switch (stat)
        {
            case BaseStat.StatTypes.HP:
            player.baseStat[0].baseValue += amount;
            break;
            
            case BaseStat.StatTypes.ATK:
            player.baseStat[1].baseValue += amount;
            break;
            
            default:
            break;
        }
        
    }
    public void addDoranStat(BaseStat.StatTypes stat,int amount){
        switch (stat)
        {
            case BaseStat.StatTypes.ATK:
            doranStat.baseStat[0].baseValue += amount;
            break;
            
            case BaseStat.StatTypes.CRIT:
            doranStat.baseStat[1].baseValue += amount;
            break;
            
            case BaseStat.StatTypes.DEF:
            doranStat.baseStat[2].baseValue += amount;
            break;
            
            case BaseStat.StatTypes.HP:
            doranStat.baseStat[3].baseValue += amount;
            break;
            
            default:
            break;
        }
        
    }
    public bool isEmptySpot(){
        if(player.ownedSpots.Count > 0){
            return false;
        }else{
            return true;
        }
    }
    public bool isEmptySpot(StopPoint.StopTypes type){
        foreach(StopPoint points in player.ownedSpots){
            if(points.stopType == type){
                return false;
            }
        }
        return true;
    }
    public bool isEmptyElem(){
        if(player.currentFire>0 ||
            player.currentIce>0 ||
            player.currentLeaf>0 ||
            player.currentThunder>0 ){
            return false;
        }else{
            return true;
        }
    }
    public bool isEmptyElem(StopPoint.StopTypes type){
        switch(type){
            case StopPoint.StopTypes.FIRE:
                if(player.currentFire <= 0)
                return true;
            break;
            case StopPoint.StopTypes.ICE:
                if(player.currentIce <= 0)
                return true;
            break;
            case StopPoint.StopTypes.LEAF:
                if(player.currentLeaf <= 0)
                return true;
            break;
            case StopPoint.StopTypes.THUNDER:
                if(player.currentThunder <= 0)
                return true;
            break;
        }
        return false;
    }
    public void addElem(int amount,StopPoint.StopTypes type){
        switch(type){
            case StopPoint.StopTypes.FIRE:
            player.currentFire+=amount;
            break;
            case StopPoint.StopTypes.ICE:
            player.currentIce+=amount;
            break;
            case StopPoint.StopTypes.LEAF:
            player.currentLeaf+=amount;
            break;
            case StopPoint.StopTypes.THUNDER:
            player.currentThunder+=amount;
            break;
        }
        mainCanvas.GetComponent<CanvasScript>().updateElemText(player);
    }
    public void setHasFruit(StopPoint.StopTypes type){
        switch(type){
            case StopPoint.StopTypes.FIRE:
            player.has_cili = true;
            break;
            case StopPoint.StopTypes.ICE:
            player.has_kelapa = true;
            break;
            case StopPoint.StopTypes.LEAF:
            player.has_nangka = true;
            break;
            case StopPoint.StopTypes.THUNDER:
            player.has_bawang = true;
            break;
        }
         mainCanvas.GetComponent<CanvasScript>().updateElemStatus(this);
    }
    public void removeAllFruit(){
        player.has_cili = false;
        player.has_bawang = false;
        player.has_kelapa = false;
        player.has_nangka = false;
        mainCanvas.GetComponent<CanvasScript>().updateElemStatus(this);
    }
    public bool checkPlayerHasFruit(StopPoint.StopTypes type){
        switch(type){
            case StopPoint.StopTypes.FIRE:
            if(player.has_cili)
                return true;
            else
                return false;
            case StopPoint.StopTypes.ICE:
            if(player.has_kelapa)
                return true;
            else
                return false;
            case StopPoint.StopTypes.LEAF:
            if(player.has_nangka)
                return true;
            else
                return false;
            case StopPoint.StopTypes.THUNDER:
            if(player.has_bawang)
                return true;
            else
                return false;
        }
        return false;
    }
    public bool checkPlayerHasAllFruit(){
        if(player.has_cili &&
        player.has_bawang &&
        player.has_kelapa &&
        player.has_nangka)
            return true;
        else
            return false;
    }
    public void addBuah(int amount,StopPoint.StopTypes type){
        switch(type){
            case StopPoint.StopTypes.FIRE:
            player.amt_cili+=amount;
            break;
            case StopPoint.StopTypes.ICE:
            player.amt_kelapa+=amount;
            break;
            case StopPoint.StopTypes.LEAF:
            player.amt_nangka+=amount;
            break;
            case StopPoint.StopTypes.THUNDER:
            player.amt_bawang+=amount;
            break;
        }
        mainCanvas.GetComponent<CanvasScript>().updateBuahText(player);
    }
    public void addOwnedSpot(StopPoint point){
        player.ownedSpots.Add(point);
    }
    public void removeOwnedSpot(StopPoint point){
        foreach(StopPoint p in player.ownedSpots){
            if(p.Equals(point.name)){
                player.ownedSpots.Remove(p);
            }
        }
    }
	public void addDurian(int amount){
        current_durian += amount;
        player.currentDurian = current_durian;
        updateDurianText();
    }
    public void addMonsterKilledCount(int id){
        player.monsterKilled.Add(id);
        updateProgressBar(1);
    }
    public void minusDurian(int amount){
        current_durian -= amount;
        player.currentDurian = current_durian;
        updateDurianText();
    }
    public int getDurianAmount(){
        return player.currentDurian;
    }
    public void changeEggtoMon(int num){
        player.baseMonster[num].monsterType = BaseEnemy.MonsterTypes.NORMAL;
        
        Transform pos1 = GameObject.FindGameObjectWithTag("Pet").transform.GetChild(num);
        
        GameObject pet = Instantiate(Resources.Load<GameObject>("Prefabs/Pets/togesmall")) as GameObject;
        pet.transform.SetParent(pos1,false);
        Texture blue = (Texture) Resources.Load("Material/Ubee_air");
        pet.transform.GetChild(1).GetComponent<Renderer>().material.SetTexture("_MainTex",blue);
    }
    public void updateDurianText(){
        //mainCanvas.GetComponent<CanvasScript>().refreshContent();
        //update text based on which player durian
        if(durian_amount!= null)
            durian_amount.text = "" + current_durian;
    }
    
    void updateProgressBar(int amount){
        float value = (float)amount / (float)monsterTotalTarget ;
        value *= 100;
        //upgradePanel.transform.GetChild(CanvasScript.PROGRESS_BAR).GetComponent<Slider>().value += value;
        monsterTarget -= amount;
        //upgradePanel.transform.GetChild(CanvasScript.PROGRESS_BAR).GetChild(2).GetComponent<Text>().text = "Destroy "+monsterTarget+" monsters";
    }

    
}
