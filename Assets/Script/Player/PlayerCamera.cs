﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

	public Transform pos1;
    public Transform pos2;
	public Transform camerapos;
	Vector3 pos;
    float speed = 0.1F;
	float timer = 0;
	public GameObject player,spawnManager;
	Vector3 offset;
	float smoothing = 5f;
	public GameObject firstSpot;
	TurnManager turnManager;
	void Start(){

		spawnManager = GameObject.FindGameObjectWithTag("SpawnManager");
		turnManager = GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>();
		player = turnManager.playerList[0];
		setInitialCamera();
	}
	void setInitialCamera(){
		offset = transform.position - firstSpot.transform.position;
		changeCameratoPlayer();
	}
	public void setTarget(GameObject temp){
		player = temp;
	}
	void Update()
    {
		follow();
		
    }
	
	public void stopFollow(){
		transform.GetChild(0).LookAt(spawnManager.GetComponent<SpawnManager>().currentSpawnPoint);
	}
	void changeCameratoPlayer(){
		if(player!=null){
			Vector3 targetCamPos = player.transform.position + offset;
		 	transform.position = targetCamPos;
		}
		 
	}
	void follow(){
		if(player!=null){
			Vector3 targetCamPos = player.transform.position + offset;
		 	transform.position = Vector3.Lerp (transform.position, targetCamPos, smoothing * Time.deltaTime);
		}
		 
	}
	public void followPlayer(){
		//Vector3 newpos = (transform.position - player.transform.position).normalized * distance + player.transform.position;
		//pos = new Vector3(player.transform.position.x+1,player.transform.position.y+2,player.transform.position.z+2);
		//transform.GetChild(0).LookAt(pos);

		Vector3 newpos = new Vector3(Mathf.Lerp(transform.position.x,camerapos.position.x, Time.deltaTime*0.8f),
		camerapos.position.y,
		Mathf.Lerp(transform.position.z,camerapos.position.z, Time.deltaTime*0.8f));
		transform.position = newpos;
	}

	//not using code
	/*
		if (Input.touchCount == 1)
        {
			Touch[] touches = Input.touches;
			if(touches[0].phase == TouchPhase.Moved){
				//move camera based on touch
				Vector2 delta = touches[0].deltaPosition;
				
				if(!FOLLOW_CHAR){
					if(transform.position.x <= 150 && transform.position.x >= -120)
						transform.position += new Vector3(-delta.x * moveSentivityX,0,0);
					else{
						if(transform.position.x > 150)
							transform.position = new Vector3(150,transform.position.y,transform.position.z);
						if(transform.position.x < -120)
							transform.position = new Vector3(-120,transform.position.y,transform.position.z);
					}
				}else{
					follow();
				}
				
			}
			
		}
		*/
		/*
        // pinch to zoom...
        if (Input.touchCount == 2)
        {
			Camera cam = transform.GetChild(0).gameObject.GetComponent<Camera>();
            // Store both touches.
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            // Find the position in the previous frame of each touch.
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            // Find the magnitude of the vector (the distance) between the touches in each frame.
            float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

            // Find the difference in the distances between each frame.
            float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

            // If the camera is orthographic...
            if (cam.orthographic)
            {
                // ... change the orthographic size based on the change in distance between the touches.
                cam.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

                // Make sure the orthographic size never drops below zero.
                cam.orthographicSize = Mathf.Max(cam.orthographicSize, 0.1f);
            }
            else
            {
                // Otherwise change the field of view based on the change in distance between the touches.
                cam.fieldOfView += deltaMagnitudeDiff * perspectiveZoomSpeed;

                // Clamp the field of view to make sure it's between 0 and 180.
                cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, 0.1f, 89.9f);
            }
        }
		*/
		/*
		// Update is called once per frame
		void FixedUpdate () {
			//pos = new Vector3(player.transform.position.x+1,player.transform.position.y+2,player.transform.position.z+2);
			//transform.LookAt(pos);
			//repeat left and right
			timer += Time.deltaTime;
			//Debug.Log("time "+timer);
			if(timer > 10f){
				timer = 0;
				lookLeft = !lookLeft;
				//float random = Random.Range(0f,1f);
				//GetComponent<Animator>().SetFloat("look",random);
			}

			if(ROTATE){
				stopFollow();
			}
			else{
				follow();
			}
			
		}
		*/
}
