﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerAttack : MonoBehaviour {

    //dummy data
    public float attackRange;
    public float cooldown_atk;
	public float timer,timer2;
    public int combo_count = 0;
    
    public GameObject backHolder,weaponHolder;
    
    public GameObject[] duriWeapon;
    Animator anim;
    
    GameObject[] enemies;
    GameObject enemyTarget;
    GameObject battleMode,mainCanvas;
    public bool DURI_MODE = false;
    bool IsFirstTime;
    CanvasScript canvasScript;
    BattleManager battleManager;
    
	void Start(){
        duriMode(false);
        anim = GetComponent<Animator>();
        mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        if(mainCanvas!= null){
            battleMode = mainCanvas.transform.GetChild(CanvasScript.BATTLE_MODE).gameObject;
            canvasScript = mainCanvas.GetComponent<CanvasScript>();
            battleManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleManager>();
        }
        IsFirstTime = true;
    }
	void Update () {
        //timer for cooldown
        timer += Time.deltaTime;
        
        if(DURI_MODE){
            timer2 += Time.deltaTime;
            if(timer2 > 10)
                duriMode(false);
        }
	}
	public GameObject findEnemy(GameObject player){
         GameObject target = null;
         enemies = GameObject.FindGameObjectsWithTag("Enemy");
         if(enemies.Length > 0){
             foreach(GameObject enemy in enemies){
                 
                float distance = Vector3.Distance(enemy.transform.position,player.transform.position);
                if(distance < attackRange){
                    target = enemy;
                }
                if(target == enemy){
                    return target.gameObject;
                }
            }
         }
         return null;
    }
    public GameObject findEnemyBoss(GameObject player){
         GameObject target = null;
         enemies = GameObject.FindGameObjectsWithTag("EnemyBoss");
         if(enemies.Length > 0){
             foreach(GameObject enemy in enemies){
                 
                float distance = Vector3.Distance(enemy.transform.position,player.transform.position);
                if(distance < attackRange){
                    target = enemy;
                }
                if(target == enemy){
                    //player.transform.LookAt (target.transform.position);
                    return target.gameObject;
                }
            }
         }
         return null;
    }
    public void Attack_Normal(GameObject player,GameObject target){
        
        Animator anim = player.transform.GetChild(0).GetComponent<Animator>();
        //if(!anim.GetBool("IsAttackingRandom"))
        //{
        if (target != null) {
            player.transform.LookAt (target.transform.position);
            equipWeapon(true);
            //float anim_atk = Random.Range(0,1.0f);
            if(combo_count > 3)
                combo_count = 0;

            anim.SetFloat("Attack",combo_count);
            timer = 0;
            combo_count++;
            
        }
        anim.SetTrigger("IsAttackingRandom");
        anim.SetBool("IsAttack",true);
        //}
        
    }
    public void SetEnemyTarget(GameObject target){
       enemyTarget = target;
    }
    public void FindNewTarget(){
       enemyTarget = findEnemy(transform.parent.gameObject);
    }
    public void Attack(){
        
        if (enemyTarget != null) {
            transform.parent.LookAt (enemyTarget.transform.position);
            equipWeapon(true);
            float anim_atk = Random.Range(0,1.0f);
            anim.SetFloat("Attack",anim_atk);
            int damage = transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).baseValue + 
                        transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).modifiedValue;
            enemyTarget.GetComponent<Enemy>().TakeDamage(transform.parent.gameObject,damage);
            anim.SetTrigger("IsAttackingRandom");
            anim.SetBool("IsAttack",true);
            battleManager.addDurianToBucket();

            if(IsFirstTime){
                canvasScript.showPanelText("Find same color durian to unleash skill!");
                IsFirstTime = false;
            }
                
            
        }else{
            Debug.Log("No Enemy Sighted! ");
            FindNewTarget();

        }
        StartCoroutine(battleManager.endTurn());
        
    }
    
    public void ExitAttack()
	{
        //equipWeapon(false);
        weaponHolder.GetComponent<BoxCollider>().enabled = false;
	}
    public void DequipWeapon()
	{
        equipWeapon(false);
        GetComponent<Animator>().SetBool("IsAttack",false);
	}
    public void AttackColliderOn(){
        weaponHolder.GetComponent<BoxCollider>().enabled = true;
    }
    public void equipWeapon(bool equip){
        if(equip && !DURI_MODE){
            backHolder.SetActive(false);
            weaponHolder.SetActive(true);
        }else{
            backHolder.SetActive(true);
            weaponHolder.SetActive(false);
        }
    }
    public void durianSkill(){

        if(battleManager.checkbucketList() > 1){
            //perform durian skill
            switch(battleManager.getBucket()[0]){
                case "blue":
                    if(battleManager.getBucket()[1] == "blue"){
                        canvasScript.showPanelText("Hurricane Slash!");
                        battleMode.transform.GetChild(3).GetComponent<Animator>().SetTrigger("activateskill");
                        StartCoroutine(ActivateSkill());
                    }else{
                         Attack();
                    }
                break;
                case "red":
                    if(battleManager.getBucket()[1] == "red"){
                        canvasScript.showPanelText("Hurricane Slash!");
                        battleMode.transform.GetChild(3).GetComponent<Animator>().SetTrigger("activateskill");
                        StartCoroutine(ActivateSkill());
                    }else{
                         Attack();
                    }
                break;
                case "yellow":
                    if(battleManager.getBucket()[1] == "yellow"){
                        canvasScript.showPanelText("Hurricane Slash!");
                        battleMode.transform.GetChild(3).GetComponent<Animator>().SetTrigger("activateskill");
                        StartCoroutine(ActivateSkill());
                    }else{
                        canvasScript.showPanelText("Normal Attack?");
                        Attack(); 
                    }
                break;
            }
            battleManager.removeDurianFromBucket(2);
            StartCoroutine(battleManager.endTurn());
         }
    }
    public int IsSkillAvailable(){
        DoranState state = transform.parent.GetComponent<DoranState>();
        if(battleManager.checkbucketList() > 1){
            switch(battleManager.getBucket()[0]){
                case "blue":
                    if(battleManager.getBucket()[1] == "blue"){
                        return 2;
                    }
                break;
                case "red":
                    if(battleManager.getBucket()[1] == "red"){
                        return 2;
                    }
                break;
                case "yellow":
                    if(battleManager.getBucket()[1] == "yellow"){
                        return 2;
                    }
                break;
            }
            return 1;
        }else{
            return 0;
        }
    }
    IEnumerator ActivateSkill() {
        GetComponent<Animator>().SetTrigger("isSkill");
        yield return new WaitForSeconds(1);
         //HURICCANE SLASH
        GameObject attack = Instantiate(Resources.Load<GameObject>("Particle/Wind")) as GameObject;
        attack.transform.SetParent(transform, false);
        attack.AddComponent<FairyLaser>().shootHurricane(transform.parent.gameObject,enemyTarget);
        Destroy(attack,1);
        //temporary damage
        int damage = transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).baseValue + 
        transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).modifiedValue;
        enemyTarget.GetComponent<Enemy>().TakeDamage(transform.parent.gameObject,200+damage);
    }
    public void duriMode(bool duriMode){
        
        DURI_MODE = duriMode;
        if(duriMode){
            if(battleManager.checkbucketList() > 1){
                duriWeapon[0].SetActive(true);
                duriWeapon[1].SetActive(true);
                battleManager.removeDurianFromBucket(2);
                transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).StatModifiedValue = 20;
                StartCoroutine(battleManager.endTurn());
            }
        }else{
            timer2 = 0;
            duriWeapon[0].SetActive(false);
            duriWeapon[1].SetActive(false);
            transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.ATK).StatModifiedValue = 0;
        }
    }
    public void Defence(){
        battleMode.transform.GetChild(3).GetComponent<SoundPlayScript>().playDefendAudio();
        DoranState state = transform.parent.GetComponent<DoranState>();
        if(battleManager.checkbucketList() > 0){
            
            canvasScript.showPanelText("Select which durian to consume");
            //toggle durian
            Transform bucket = battleMode.transform.GetChild(3).GetChild(1);
            for(int i=0;i<5;i++){
                if(bucket.GetChild(i).childCount > 0){
                    GameObject durian = bucket.GetChild(i).GetChild(0).gameObject;
                    
                    StartCoroutine(SetDurianButton(durian));
                }
            }
            //transform.parent.GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.DEF).StatModifiedValue += 5;
            //transform.parent.GetComponent<DoranState>().removeDurianFromBucket(1);
            //StartCoroutine(transform.parent.GetComponent<DoranState>().endTurn());
        }else{
            canvasScript.showPanelText("You need to have at least 1 durian!");
        }
    }
    IEnumerator SetDurianButton(GameObject durian) {
        durian.GetComponent<Animator>().SetTrigger("toggledurian");
        durian.GetComponent<Button>().interactable = true;
        yield return new WaitForSeconds(2);
        durian.GetComponent<Button>().interactable = false;
    }
    
}
