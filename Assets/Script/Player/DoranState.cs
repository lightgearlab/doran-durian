﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;
using UnityEngine.AI;
public class DoranState : MonoBehaviour {
    
	private Rigidbody _rigidBody;
	Animator anim;
    NavMeshAgent nav;
    GameObject maincanvas;
    //public Transform[] spawnPoints,stopPoints;
    GameObject moveCanvas,upgradepanel,particleCanvas,battleManagerObj,turnManagerObj;
    DoranPlayer doranPlayer;
    BattleManager battleManager;
    TurnManager turnManager;
    bool IsAtSpawnPoint,IsFirstTime,CHANGE_STATE;
    int currentPosNumber;
    public GameObject currentSpot;
    
    // [HideInInspector]
    public Transform move_left,move_right,move_down,move_up;
    [HideInInspector]
    public Transform moveleft_btn,moveright_btn,moveupdown_btn;
    
	public enum State {
        START,
        MOVING,
        IDLE,
        DICE,
        DICE_MOVE,
        BATTLE,
        CHOOSE,
        WAIT,
        BATTLE_HARD,
        BATTLE_SPECIAL,
        ITEM,
        HEAL,
        BOSS,
        GODOWN,
        SKILL,
        KITCHEN,
        WORKSHOP,
        TUTORIAL,
        NOTUTORIAL,
        END
    }
    public State currentState;
    public int moveCount = 0;
    [HideInInspector]
    public List<Transform> traveledPoints;
	void Awake()
	{
        _rigidBody = GetComponent<Rigidbody>();
		anim = transform.GetChild(0).GetComponent<Animator> ();
        nav = GetComponent<NavMeshAgent>();
        
        //update next module
        // SpawnManager spawnManager = GameObject.FindGameObjectWithTag("SpawnManager").GetComponent<SpawnManager>();
        // spawnManager.spawnNewStage();
        maincanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        if(maincanvas!=null){
            moveCanvas = maincanvas.transform.GetChild(12).gameObject;
            moveleft_btn = moveCanvas.transform.GetChild(0);
            moveright_btn = moveCanvas.transform.GetChild(2);
            moveupdown_btn = moveCanvas.transform.GetChild(1);
            
            moveleft_btn.GetComponent<Image>().sprite = ImageManager.getSpriteFromSpriteSheet("UI/gamegui","gamegui_4");
            moveright_btn.GetComponent<Image>().sprite = ImageManager.getSpriteFromSpriteSheet("UI/gamegui","gamegui_4");
            moveupdown_btn.GetComponent<Image>().sprite = ImageManager.getSpriteFromSpriteSheet("UI/gamegui","gamegui_4");
            moveleft_btn.rotation = Quaternion.Euler(0,0,0);
            moveright_btn.rotation = Quaternion.Euler(0,0,180);
        }
        
        
        doranPlayer = GetComponent<DoranPlayer>();
        upgradepanel = GameObject.FindGameObjectWithTag("UpgradePanel");
        particleCanvas =  GameObject.FindGameObjectWithTag("ParticleCanvas");
        battleManagerObj = GameObject.FindGameObjectWithTag("BattleManager");
        turnManagerObj = GameObject.FindGameObjectWithTag("TurnManager");
        if(battleManagerObj!=null)battleManager = battleManagerObj.GetComponent<BattleManager>();
        if(turnManagerObj!=null)turnManager = turnManagerObj.GetComponent<TurnManager>();

        CHANGE_STATE = false;
        IsFirstTime = true;
        traveledPoints = new List<Transform>();

        
        
	}

	void Update () {
        //if(isActive)
        //    isPlayerTurn();
	}
    public void isPlayerTurn(){
        //Debug.Log(gameObject.name+" "+currentState);
        switch (currentState)
        {
            case State.START:
            if(move_down == null && move_left == null 
                && move_right == null && move_up == null){
                    MoveToStopPoint(turnManager.startPoint);
            }
            break;

            case State.MOVING:

            if(CHANGE_STATE){
                playerIsNotIdle();
                CHANGE_STATE = false;
            }else{
               // Debug.Log("Error");
            }
            break;
            case State.IDLE:
            
            if(!CHANGE_STATE){
                    
                stayStill();
                CHANGE_STATE = true;
            }else{
                Debug.Log("Error");
            }
            break;
            case State.DICE:
            
            if(!CHANGE_STATE){
                    
                stayStill();
                
                CHANGE_STATE = true;
            }else{
                Debug.Log("Error");
            }
            break;
            case State.DICE_MOVE:
            
            if(!CHANGE_STATE){
                    
                stayStill();
                
                playerAtStopPoint();
                
                CHANGE_STATE = true;
            }else{
                Debug.Log("Error");
            }
            break;

            case State.BATTLE:

                stayStill();

                if(!CHANGE_STATE){
                    
                    if(IsFirstTime){
                        //battleManager.showTutorial();
                    }
                    //set turn for 5 turns
                    battleManager.addTurnBasedOnSpeed();
                    
                    changeStateAndExecuteTurn(State.CHOOSE);
                }else{
                    changeStateAndExecuteTurn(State.IDLE);
                }
            break;

            case State.WAIT:

            if(!CHANGE_STATE){
                // PlayerAttack player_attack3 = transform.GetChild(0).GetComponent<PlayerAttack>();
                // GameObject enemy = player_attack3.findEnemy(gameObject);
                // if(enemy != null){
                //     battleManager.monsterTurn(enemy);
                //     changeStateAndExecuteTurn(State.CHOOSE);
                // }else{
                //}
                changeStateAndExecuteTurn(State.IDLE);
                playerAtStopPoint();
                IsFirstTime = false;
                CHANGE_STATE = true;
            }
            

            break;

            case State.CHOOSE:

            
            if(!CHANGE_STATE){
                PlayerAttack player_attack3 = transform.GetChild(0).GetComponent<PlayerAttack>();
                GameObject enemy = player_attack3.findEnemy(gameObject);
                if(enemy == null){
                    changeStateAndExecuteTurn(State.IDLE);
                }else{
                    player_attack3.SetEnemyTarget(enemy);
                    playerIsBattle();
                    battleManager.showPlayerSelection();
                }
                CHANGE_STATE = true;
            }
            
            break;
            
            default:
            break;
        }
    }
    public bool checkPlayerAtStopPoint(){
        bool playerAtStopPoint = false;
        if(move_down != null && move_left != null 
                && move_right != null && move_up != null){
            playerAtStopPoint = true;
        }
        return playerAtStopPoint;
    }
     void playerIsNotIdle(){
        maincanvas.transform.GetChild(CanvasScript.SKILL).gameObject.SetActive(false);
        if(moveCanvas.activeSelf)
            moveCanvas.SetActive(false);
     }
     //rosak kat sini
     void playerAtStopPoint(){
        //Debug.Log(gameObject.name+" At Stop Point move= " + moveCount);
        if(moveCount > 0)
            currentSpot.GetComponent<StopPoint>().setMoveset(gameObject);
        else
            currentSpot.GetComponent<StopPoint>().removeMoveset(gameObject);

        if(!moveCanvas.activeSelf)
            moveCanvas.SetActive(true);
        moveleft_btn.GetComponent<Button>().onClick.RemoveAllListeners();
        moveright_btn.GetComponent<Button>().onClick.RemoveAllListeners();
        moveupdown_btn.GetComponent<Button>().onClick.RemoveAllListeners();
        //at stop point
        moveleft_btn.gameObject.SetActive(false);
        moveupdown_btn.gameObject.SetActive(false);
        moveright_btn.gameObject.SetActive(false);
        
        if(move_left!=null){
            moveleft_btn.gameObject.SetActive(true);
            moveleft_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_left); });
        }  
        if(move_right!=null){
            moveright_btn.gameObject.SetActive(true);
            moveright_btn.rotation = Quaternion.Euler(0,0,180);
            moveright_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_right); });
        }
        if(move_down!=null && move_up!=null){
            moveright_btn.gameObject.SetActive(true);
            moveright_btn.rotation = Quaternion.Euler(0,0,90);
            moveright_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_down); });
            moveupdown_btn.gameObject.SetActive(true);
            moveupdown_btn.rotation = Quaternion.Euler(0,0,270);
            moveupdown_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_up); });
        }else{
            if(move_down!=null){
                moveupdown_btn.gameObject.SetActive(true);
                moveupdown_btn.rotation = Quaternion.Euler(0,0,90);
                moveupdown_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_down); });
            }
            if(move_up!=null){
                moveupdown_btn.gameObject.SetActive(true);
                moveupdown_btn.rotation = Quaternion.Euler(0,0,270);
                moveupdown_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_up); });
            }
        }
        //not using atm
        //check which area
        // if(IsAtSpawnPoint){
        //     moveleft_btn.gameObject.SetActive(false);
        //     moveupdown_btn.gameObject.SetActive(true);
        //     moveright_btn.gameObject.SetActive(false);

        //     switch(currentPosNumber){

        //         default:
        //         moveupdown_btn.rotation = Quaternion.Euler(0,0,90);
        //         moveupdown_btn.GetComponent<Button>().onClick.AddListener(delegate { MoveToStopPoint(move_down); });
        //         break;
        //     }
            
        // }else{
            
        // }
     }
     void playerIsBattle(){
        playerIsNotIdle();
        battleManager.showBattleMenu();
        switch(doranPlayer.transform.GetChild(0).GetComponent<PlayerAttack>().IsSkillAvailable()){
            case 0:
            //not enough durian to combine
            //battleMode.transform.GetChild(0).GetChild(2).GetComponent<Button>().interactable = false;
            break;
            case 1:
            //normal attack
            //battleMode.transform.GetChild(0).GetChild(2).GetComponent<Button>().interactable = true;
            break;
            case 2:
            //possible skill
            //battleMode.transform.GetChild(0).GetChild(2).GetComponent<Button>().interactable = true;
            
            if(IsFirstTime){
                maincanvas.GetComponent<CanvasScript>().showPanelText("Choose Your Actions!");
            }
            break;
        }
     }
    public void changeStateAndExecuteTurn(State state){
        //Debug.Log(gameObject.name+" "+currentState+"->"+state);
        CHANGE_STATE = true;
        currentState = state;
        CHANGE_STATE = false;
        isPlayerTurn();
    }
    public void changeState(State state){
        //Debug.Log(gameObject.name+" "+currentState+"->"+state);
        CHANGE_STATE = true;
        currentState = state;
        CHANGE_STATE = false;
    }
    public void atSpawnPoint(int num){
        IsAtSpawnPoint = true;
        currentPosNumber = num;
    }
    public void atStopPoint(int num){
        IsAtSpawnPoint = false;
        currentPosNumber = num;
        
    }
    // public void useDurianAndDisable(int num){
    //     if(player.getDurianAmount() > num)
    //     {
    //         IsBlocked = false;
    //         player.minusDurian(num);
    //         moveCanvas.transform.GetChild(1).gameObject.SetActive(true);
    //         GameObject.FindGameObjectWithTag("SpawnManager").transform.GetChild(0).gameObject.SetActive(false);
    //         setIdleModeOn(true);
    //     }
        
    // }
    
    public void MoveToStopPoint(Transform point){
        if(nav!=null){
            changeStateAndExecuteTurn(State.MOVING);
            nav.Resume();
            Transform pos = point;
            Vector3 post = new Vector3(pos.position.x,pos.position.y,pos.position.z);
            nav.SetDestination(post);
            anim.SetFloat ("Move",Mathf.Abs(5));
        }else{
            Debug.Log("Nav is Null at MoveToStopPoint:"+gameObject.name);
        }
    }
    public void stayStill(){
        if(nav!=null){
            nav.Stop();
            anim.SetFloat ("Move",Mathf.Abs(0));
        }else{
            Debug.Log("Nav is Null at stayStill:"+gameObject.name);
        }
            
    }
    public void endMyTurn(){
        turnManager.endPlayerTurnNSetNextPlayerTurn();
    }
    public void addRemoveMoveCount(int i){
        //Debug.Log("addRemoveMoveCount "+gameObject.name+" "+i);
        moveCount +=i;
        updateMoveCountText();
    }
    public void setMoveCount(int i){
        //Debug.Log("setMoveCount "+gameObject.name+" "+i);
        moveCount = i;
        updateMoveCountText();
    }
    public void updatePlayerText(){
        moveCanvas.transform.GetChild(3).GetChild(1).GetComponent<Text>().text = gameObject.name; 
    }
    public void updateMoveCountText(){
        moveCanvas.transform.GetChild(3).GetChild(0).GetComponent<Text>().text = ""+moveCount;
    }
    public IEnumerator EndTurn(float time) {
        yield return new WaitForSeconds(time);
        endMyTurn();
    }
}
