﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerHealth : MonoBehaviour {

    Slider hp_slider;
    Text hp_text;
    float currentHealth,maxValue;
    AudioSource audioSource;
    GameObject upgradePanel,canvas;
	void Start () {
        initialize();
	}
    public void initialize(){
        upgradePanel = GameObject.FindGameObjectWithTag("UpgradePanel");
        canvas = GameObject.FindGameObjectWithTag("MainCanvas");
        audioSource = gameObject.AddComponent<AudioSource>();
    }
	public void initializeHP(){
        if(upgradePanel.activeSelf){
            //hp_slider = upgradePanel.transform.GetChild(0).GetComponent<Slider>();
            //hp_text = upgradePanel.transform.GetChild(0).GetChild(2).GetComponent<Text>();
            hp_slider = transform.GetChild(1).GetChild(0).GetComponent<Slider>();
            //hp_slider = canvas.transform.GetChild(9).GetComponent<Slider>();
            hp_text = canvas.transform.GetChild(9).GetChild(2).GetComponent<Text>();
            currentHealth = GameObject.FindGameObjectWithTag("Player").GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.HP).baseValue;
            maxValue = currentHealth;
            hp_slider.maxValue = currentHealth;
            hp_slider.value = currentHealth;
            hp_text.text = ""+currentHealth + "/" + maxValue+" HP";
        }
    }
    public void TakeDamage(float damage){
        
        if(currentHealth < 30){
            Image health = hp_slider.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>();
            health.color = Color.red;
            //NotificationScript.showNotification("Doran's health getting low! Heal him by changing mode to HEAL!");
        }else{
            Image health = hp_slider.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>();
            health.color = Color.green;
        }
        //damage doran
        currentHealth -= damage;
        hp_slider.value = currentHealth;
        hp_text.text = ""+currentHealth + "/" + maxValue+" HP";
        transform.GetChild(1).GetComponent<DamagePopups>().CreateDamagePopup((int)damage);
        
        //show particle
        GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Blast")) as GameObject;
        skillparticle.transform.SetParent(gameObject.transform, false);
        Destroy(skillparticle,2f);
        
        //if doran die, restart level        
        if(currentHealth <= 0){
            GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetComponent<Animator>().SetBool("IsDead",true);
            StartCoroutine("changeSceneandWait");
        }
    }
     IEnumerator changeSceneandWait()
    {
        yield return new WaitForSeconds(2);
        SpawnManager.TUTORIAL = false;
        ChangeScene.playAnimation();
        //GameObject.FindGameObjectWithTag("Player").transform.GetChild(0).GetComponent<Animator>().SetBool("IsDead",false);
        //initializeHP();
        //ChangeScene.playTutorial();
        //ChangeScene.changeScene(ChangeScene.SCENE_BATTLE);
     }
    public void Heal(float amount){
        if(currentHealth < 30){
            //Image health = hp_slider.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>();
            //health.color = Color.red;
        }else{
            Image health = hp_slider.transform.GetChild(1).transform.GetChild(0).GetComponent<Image>();
            health.color = Color.green;
        }
        float max = GameObject.FindGameObjectWithTag("Player").GetComponent<DoranPlayer>().getDoranStat(BaseStat.StatTypes.HP).baseValue;
        maxValue = max;
        if(currentHealth < max){
            audioSource.clip = Resources.Load<AudioClip>("Audio/DurianSound");
            audioSource.Play();
            
            currentHealth += amount;
            transform.GetChild(1).GetComponent<DamagePopups>().CreateHealPopup((int)amount);
            if(currentHealth > max)
                currentHealth = max;
            hp_slider.value = currentHealth;
            hp_text.text = ""+currentHealth + "/" + maxValue+" HP";
        }
    }
	// Update is called once per frame
	void Update () {
	    
	}
}
