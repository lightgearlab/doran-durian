﻿using UnityEngine;
using System.Collections;

public class Pet : MonoBehaviour {
	float cooldown_atk = 3f;
    float timer = 0;
	GameObject player;
	Animator anim;
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag("Player");
 		anim = gameObject.GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
        
        if (timer >= cooldown_atk )
        {
            petAttack();
            timer = 0;
        }
	}
	void petAttack(){
		
		PlayerAttack player_attack = player.transform.GetChild(0).GetComponent<PlayerAttack>();
		GameObject enemy = player_attack.findEnemy(player);
		GameObject enemy_boss = player_attack.findEnemyBoss(player);
		anim.SetTrigger("IsAttacking");
		
		//pet attack
		if(enemy != null){
				
				GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Spark"),enemy.transform.position,Quaternion.identity) as GameObject;
				skillparticle.transform.SetParent(enemy.transform);
				
				//int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
				//TODO: Setup pet damage
				enemy.GetComponent<Enemy>().TakeDamageFairy(enemy,20);
			
		}else if(enemy_boss != null)
		{
				GameObject skillparticle = Instantiate(Resources.Load<GameObject>("Particle/Spark"),enemy_boss.transform.position,Quaternion.identity) as GameObject;
        		skillparticle.transform.SetParent(enemy_boss.transform);
				
				//int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
				enemy_boss.GetComponent<Enemy>().TakeDamageFairyBoss(enemy_boss,20);
		}
	}
}
