﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;

public class DiceScript : MonoBehaviour,IPointerClickHandler {

	public GameObject cursor;
	public GameObject[] numberList;
	DoranState playerState;
	TurnManager turnManager;
	int num;
	private IEnumerator rotate;
	bool not_yet;
	// Use this for initialization
	void Start () {
		num = 0;
		rotate = Rotate(0.05f);
		
	}
	public void OnPointerClick (PointerEventData data){
        if(!not_yet){
			stopDice();
			not_yet = true;
		}
    }
	public void startDice(){
		StartCoroutine(rotate);
		turnManager = GameObject.FindGameObjectWithTag("TurnManager").GetComponent<TurnManager>();
		playerState = turnManager.playerList[0].GetComponent<DoranState>();
		not_yet = true;
	}
	public void stopDice(){
		StopCoroutine(rotate);
		GetComponent<Animator>().SetTrigger("End");
		int temp = num;
		if(num==0)
			temp = 8;
		playerState.setMoveCount(temp);
		turnManager.diceNumber = temp;
		playerState.changeStateAndExecuteTurn(DoranState.State.DICE_MOVE);
	}
	public void showDice(){
		gameObject.SetActive(true);
	}
	public void removeDice(){
		gameObject.SetActive(false);
	}
	private IEnumerator Rotate(float waitTime) {
        while (true) {
            yield return new WaitForSeconds(waitTime);
			not_yet = false;
            TransformExtensions.LookAt2D(cursor.transform,numberList[num].transform);
			num++;
			if(num >= 8){
				num=0;
			}

        }
    }
	// Update is called once per frame
	void Update () {
		
	}
}
