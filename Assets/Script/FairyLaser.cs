﻿using UnityEngine;
using System.Collections;

public class FairyLaser : MonoBehaviour {

	Vector3 enemy_position,fairy_position;
    float dist,counter;
    LineRenderer line;
	GameObject enemy;
	bool shoot = false;
	bool hurricane = false;
	// Use this for initialization
	void Start () {
		//enemy = GameObject.FindGameObjectWithTag("Enemy");
		//fairy_transform = GameObject.FindGameObjectWithTag("Fairy").transform;
		
	}
	public void shootLaser(GameObject fairy, GameObject enemy){
		shoot = true;
		fairy_position = fairy.transform.position;
		enemy_position = new Vector3(enemy.transform.position.x,enemy.transform.position.y,enemy.transform.position.z);
		transform.position = fairy_position;
		transform.LookAt(enemy_position);
		Destroy(gameObject,0.2f);
	}
	public void shootHurricane(GameObject fairy, GameObject enemy){
		hurricane = true;
		fairy_position = fairy.transform.position;
		enemy_position = new Vector3(enemy.transform.position.x,enemy.transform.position.y,enemy.transform.position.z);
		transform.position = fairy_position;
		transform.LookAt(enemy_position);
		//enemy.transform.position += Vector3.forward*2f;
		Destroy(gameObject,2f);
	}
	// Update is called once per frame
	void Update () {
		if(hurricane)
			transform.position += transform.forward*1f*Time.deltaTime;
		if(shoot)
			transform.position += transform.forward*20f*Time.deltaTime;
			
	}
}
