﻿using UnityEngine;
using System.Collections;

public class FairySkill : MonoBehaviour {

	int damage = 100;
	int radius = 4;
	
	// Use this for initialization
	void Start () {
		GetComponent<SphereCollider>().radius = radius;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnTriggerEnter(Collider other) {
        //trigger when enemy enters
            if(other.tag == "Enemy"){
                //int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
            	other.GetComponent<Enemy>().TakeDamageFairy(other.gameObject,damage);
            }else if(other.tag == "EnemyBoss"){
                //int damage = player.GetComponent<DoranPlayer>().getStat(BaseStat.StatTypes.ATK).baseValue;
            	other.GetComponent<Enemy>().TakeDamageFairyBoss(other.gameObject,damage);
            }
    }
}
