﻿using UnityEngine;
using System.Collections;

public class SoundPlayScript : MonoBehaviour {

	public AudioClip defendAudio;
	public AudioClip skillAudio;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void playAudio(){
		GetComponent<AudioSource>().clip = skillAudio;
        GetComponent<AudioSource>().Play();
    }
	public void playDefendAudio(){
		GetComponent<AudioSource>().clip = defendAudio;
        GetComponent<AudioSource>().Play();
    }
}
