﻿using UnityEngine;
using System.Collections.Generic;
[System.Serializable]
public class BaseEnemy {
	public string monsterName;
    public int monsterId;
	public int monsterLevel;
    public int monsterExperience;
	public int attackRange;
	public int chasingRange;
    public List<BaseStat> monsterStat;
    public string ingredientDropId;
	public MonsterTypes monsterType;
    public MonsterModes monsterMode;
    public enum MonsterTypes
    {
        EGG,
        NORMAL
    }
    public enum MonsterModes
    {
        ATTACK,
        HEAL,
        HYBRID
    }
    private BaseClass baseClass; // not using atm
	

    public BaseEnemy(Dictionary<string,string> monsterDictionary) { 
        monsterName = monsterDictionary["MonsterName"];
        monsterId = int.Parse(monsterDictionary["MonsterId"]);
        monsterLevel = int.Parse(monsterDictionary["Level"]);
		attackRange = int.Parse(monsterDictionary["AttackRange"]);
		chasingRange = int.Parse(monsterDictionary["ChasingRange"]);
        monsterStat = initializeStat(monsterDictionary,monsterStat);
        ingredientDropId = monsterDictionary["IngredientDropId"];
        monsterType = (MonsterTypes) System.Enum.Parse(typeof(BaseEnemy.MonsterTypes),monsterDictionary["MonsterType"].ToString());
        monsterExperience = monsterLevel*100; // temporary multiplier
    
    }
    public BaseEnemy()
    {
    }
    public static List<BaseStat> initializeStat(Dictionary<string, string> monsterDictionary,List<BaseStat> monsterStat)
    {
        monsterStat = new List<BaseStat>();
        BaseHP hp = new BaseHP();
        BaseMP mp = new BaseMP();
        BaseATK atk = new BaseATK();
        BaseDEF def = new BaseDEF();
        BaseMOVESPD movespd = new BaseMOVESPD();
        BaseATKSPD atkspd = new BaseATKSPD();

        //temp value
        hp.StatBaseValue =  int.Parse(monsterDictionary["HP"]);
        mp.StatBaseValue = int.Parse(monsterDictionary["MP"]);
        atk.StatBaseValue = int.Parse(monsterDictionary["ATK"]);
        def.StatBaseValue = int.Parse(monsterDictionary["DEF"]);
        movespd.StatBaseValue = int.Parse(monsterDictionary["MOVESPD"]);
        atkspd.StatBaseValue = int.Parse(monsterDictionary["ATKSPD"]);

        monsterStat.Add(hp);
        monsterStat.Add(mp);
        monsterStat.Add(atk);
        monsterStat.Add(def);
        monsterStat.Add(movespd);
        monsterStat.Add(atkspd);

        return monsterStat;
    }
	//getter and setter
	public string Name{
		get {return monsterName;}
		set{monsterName = value;}
	}
    public int Id
    {
        get { return monsterId; }
        set { monsterId = value; }
    }
    public int Experience{
		get {return monsterExperience;}
		set{monsterExperience = value;}
	}
	public int Level{
		get{return monsterLevel;}
		set{monsterLevel = value;}
	}
	public int AttackRange{
		get{return attackRange;}
		set{attackRange = value;}
	}
	public int ChasingRange{
		get{return chasingRange;}
		set{chasingRange = value;}
	}
	public BaseClass MonsterClass{
		get{return baseClass;}
		set{baseClass = value;}
	}
	public MonsterTypes MonsterType{
		get{return monsterType;}
		set{monsterType = value;}
	}
    public MonsterModes MonsterMode{
		get{return monsterMode;}
		set{monsterMode = value;}
	}
    public List<BaseStat> MonsterStat
    {
        get { return monsterStat; }
        set { monsterStat = value; }
    }
    public string IngredientDropId
    {
        get { return ingredientDropId; }
        set { ingredientDropId = value; }
    }

}

