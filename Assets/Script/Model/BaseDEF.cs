﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseDEF : BaseStat {

    public BaseDEF()
    {

        StatName = "Defense";
        StatDescription = "DEF reduces monster attack significantly";
        StatType = StatTypes.DEF;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
