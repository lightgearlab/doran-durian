﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseMP : BaseStat {

    public BaseMP()
    {

        StatName = "Energy";
        StatDescription = "Energy is used to cook recipes";
        StatType = StatTypes.MP;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
