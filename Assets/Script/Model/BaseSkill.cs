using UnityEngine;
using System.Collections.Generic;
[System.Serializable]
public class BaseSkill {
    public string skillName;
    public int skillId;
    public string skillDescription;
    public string skillDescriptSmall;
    public string image;
    public float damageMultiplier;
    public int skillCost;
    public bool skillNew;
    public int skillCooldown;
    public int skillDuration;
    public int skillCurrentLevel;
    public int skillMaxLevel;
    public int skillPreAmount;
    
    public SkillTypes skillType;
    public SkillProjectiles skillProjectile;
    public SkillType2 skilltype2;
    public enum SkillTypes {
        Fire,
        Water,
        Electric,
        Normal,
        Heal,
        Ailment,
        Special
    }
    public enum SkillType2 {
        Active,
        Passive
    }
    public enum SkillProjectiles {
        Line,
        Arc,
        Dash,
        Circle
    }
    public BaseSkill()
    {
    }
    //getter and setter
    public string Name
    {
        get { return skillName; }
        set { skillName = value; }
    }
    public string Image
    {
        get { return image; }
        set { image = value; }
    }
    public int Id
    {
        get { return skillId; }
        set { skillId = value; }
    }
    public string Description
    {
        get { return skillDescription; }
        set { skillDescription = value;}
    }
    public float DamageMultiplier
    {
        get { return damageMultiplier; }
        set { damageMultiplier = value; }
    }
    public int SkillCost
    {
        get { return skillCost; }
        set { skillCost = value; }
    }
    public bool SkillNew
    {
        get { return skillNew; }
        set { skillNew = value; }
    }
    public int SkillCooldown
    {
        get { return skillCooldown; }
        set { skillCooldown = value; }
    }
    public int SkillDuration
    {
        get { return skillDuration; }
        set { skillDuration = value; }
    }
    public int SkillCurrentLevel
    {
        get { return skillCurrentLevel; }
        set { skillCurrentLevel = value; }
    }
    public int SkillMaxLevel
    {
        get { return skillMaxLevel; }
        set { skillMaxLevel = value; }
    }
    public int SkillPreAmount
    {
        get { return skillPreAmount; }
        set { skillPreAmount = value; }
    }
    public string SkillDescriptSmall
    {
        get { return skillDescriptSmall; }
        set { skillDescriptSmall = value; }
    }
    public SkillProjectiles SkillProjectile
    {
        get { return skillProjectile; }
        set { skillProjectile = value; }
    }
    public SkillType2 Skilltype2
    {
        get { return skilltype2; }
        set { skilltype2 = value; }
    }
    public SkillTypes SkillType
    {
        get { return skillType; }
        set { skillType = value; }
    }
}
