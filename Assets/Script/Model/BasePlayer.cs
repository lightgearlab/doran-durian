﻿using UnityEngine;
using System.Collections.Generic;
using System;
[Serializable]
public class BasePlayer  {
	public string name;
    public string image;
	public int level;
    public int currentStage;
    public List<int> monsterKilled;
    public int currentWeapon;
    public int currentDurian;
    public int currentTurn;
    public float durianPercentage;
    public List<BaseStat> baseStat;
    public List<BaseSkill> baseSkill;
    public List<BaseSkill> basePanel;
    public List<BaseEnemy> baseMonster;
    public List<StopPoint> ownedSpots;
    public int currentFire,currentIce,currentThunder,currentLeaf;

    public int amt_cili,amt_kelapa,amt_nangka,amt_bawang;
    public bool has_cili,has_kelapa,has_nangka,has_bawang;

    //getter and setter
    public string Name{
		get {return name;}
		set{name = value;}
	}
    public string Image{
		get {return image;}
		set{image = value;}
	}
    public int Level{
        get{return level;}
        set{level = value;}
    }
    public int CurrentDurian{
        get{return currentDurian;}
        set{currentDurian = value;}
    }
    public float DurianPercentage{
        get{return durianPercentage;}
        set{durianPercentage = value;}
    }
    public int CurrentStage{
        get{return currentStage;}
        set{currentStage = value;}
    }
    public int CurrentWeapon{
        get{return currentWeapon;}
        set{currentWeapon = value;}
    }
    public List<BaseStat> PlayerStat
    {
        get { return baseStat; }
        set { baseStat = value; }
    }
    public List<int> MonsterKilled
    {
        get { return monsterKilled; }
        set { monsterKilled = value; }
    }
    public List<BaseSkill> PlayerSkill
    {
        get { return baseSkill; }
        set { baseSkill = value; }
    }
    public List<BaseSkill> PlayerPanel
    {
        get { return basePanel; }
        set { basePanel = value; }
    }
    public List<BaseEnemy> BaseMonster
    {
        get { return baseMonster; }
        set { baseMonster = value; }
    }
}
