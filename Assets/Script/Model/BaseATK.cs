﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseATK : BaseStat {
    public BaseATK() {

        StatName = "Attack";
        StatDescription = "The ATK point increases the player's damage to monsters";
        StatType = StatTypes.ATK;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
