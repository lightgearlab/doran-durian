﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseHP : BaseStat {

    public BaseHP()
    {

        StatName = "Health";
        StatDescription = "The players health to last through the day";
        StatType = StatTypes.HP;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
