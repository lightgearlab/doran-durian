﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseMOVESPD : BaseStat {

    public BaseMOVESPD()
    {

        StatName = "Movement Speed";
        StatDescription = "MOVE SPD determines the players running speed";
        StatType = StatTypes.MOVESPD;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
