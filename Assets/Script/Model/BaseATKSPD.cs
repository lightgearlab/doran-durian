﻿using UnityEngine;
using System.Collections;
[System.Serializable]
public class BaseATKSPD : BaseStat {

    public BaseATKSPD()
    {

        StatName = "Attack Speed";
        StatDescription = "ATK SPD of the players depends on the type of weapon the player weilds.";
        StatType = StatTypes.ATKSPD;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
