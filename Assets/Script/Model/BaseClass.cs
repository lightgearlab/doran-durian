using UnityEngine;
using System.Collections.Generic;
[System.Serializable]
public class BaseClass  {
	public string className;
	public string classDescription;
    public List<BaseStat> classStat;
    public List<BaseSkill> classSkill;

	//getter and setter
	public string Name{
		get{return className;}
		set{className = value;}
	}
	public string Description{
		get{return classDescription;}
		set{classDescription = value;}
	}
	public List<BaseStat> ClassStat{
		get{return classStat; }
		set{ classStat = value;}
	}
    public List<BaseSkill> ClassSkill
    {
        get { return classSkill; }
        set { classSkill = value; }
    }
}
