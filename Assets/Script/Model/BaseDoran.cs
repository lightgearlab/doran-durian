﻿using UnityEngine;
using System.Collections.Generic;
using System;
[Serializable]
public class BaseDoran {

    public int currentWeapon;
    public List<BaseStat> baseStat;
    public List<BaseSkill> baseSkill;

    //getter and setter
    public int CurrentWeapon{
        get{return currentWeapon;}
        set{currentWeapon = value;}
    }
    public List<BaseStat> PlayerStat
    {
        get { return baseStat; }
        set { baseStat = value; }
    }
    public List<BaseSkill> PlayerSkill
    {
        get { return baseSkill; }
        set { baseSkill = value; }
    }
}
