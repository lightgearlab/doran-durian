using UnityEngine;
using System.Collections.Generic;
[System.Serializable]
public class BaseStat  {

    public string name;
    public string description;
    public int baseValue;
    public int modifiedValue;
    public StatTypes type;
    public enum StatTypes
    {
        HP,
        MP,
        ATK,
        DEF,
        MOVESPD,
        ATKSPD,
        CRIT
    }

    //setter and getter
    public string StatName
    {
        get { return name; }
        set { name = value; }
    }
    public string StatDescription
    {
        get { return description; }
        set { description = value; }
    }
    public int StatBaseValue
    {
        get { return baseValue ; }
        set { baseValue = value ; }
    }
    public int StatModifiedValue
    {
        get { return modifiedValue; }
        set { modifiedValue = value; }
    }
    public StatTypes StatType
    {
        get { return type; }
        set { type = value; }
    }

}
