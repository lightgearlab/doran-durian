﻿using UnityEngine;
using System.Collections;

public class BaseCRIT : BaseStat {

	public BaseCRIT() {

        StatName = "Critical";
        StatDescription = "Critical attack";
        StatType = StatTypes.CRIT;
        StatBaseValue = 0;
        StatModifiedValue = 0;
    
    }
}
