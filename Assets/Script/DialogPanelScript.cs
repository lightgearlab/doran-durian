﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class DialogPanelScript : MonoBehaviour {

	public Button yesButton,noButton;
	public Button fireButton,iceButton,thunderButton,leafButton;
	public GameObject seedGameobject;
	public delegate void SetupDialog();
	public event SetupDialog setupDialog;
	
	public void setText(string text){
		gameObject.transform.GetChild(0).GetComponent<Text>().text = text;
	}
	public void showSeeds(BasePlayer player){
		fireButton.transform.GetChild(1).GetComponent<Text>().text = ""+player.currentFire;
		iceButton.transform.GetChild(1).GetComponent<Text>().text = ""+player.currentIce;
		thunderButton.transform.GetChild(1).GetComponent<Text>().text = ""+player.currentThunder;
		leafButton.transform.GetChild(1).GetComponent<Text>().text = ""+player.currentLeaf;
		yesButton.gameObject.SetActive(false);
		noButton.gameObject.SetActive(false);
		seedGameobject.SetActive(true);
	}
	public void showSpots(BasePlayer player){
		int fire = 0,ice = 0,thunder = 0,leaf = 0;
		foreach(StopPoint point in player.ownedSpots){
			if(point.stopType == StopPoint.StopTypes.FIRE){
				fire++;
			}
			if(point.stopType == StopPoint.StopTypes.ICE){
				ice++;
			}
			if(point.stopType == StopPoint.StopTypes.THUNDER){
				thunder++;
			}
			if(point.stopType == StopPoint.StopTypes.LEAF){
				leaf++;
			}
		}
		fireButton.transform.GetChild(1).GetComponent<Text>().text = ""+fire;
		iceButton.transform.GetChild(1).GetComponent<Text>().text = ""+ice;
		thunderButton.transform.GetChild(1).GetComponent<Text>().text = ""+thunder;
		leafButton.transform.GetChild(1).GetComponent<Text>().text = ""+leaf;
		yesButton.gameObject.SetActive(false);
		noButton.gameObject.SetActive(false);
		seedGameobject.SetActive(true);
	}
	public void showDialog(){
		gameObject.SetActive(true);
		seedGameobject.SetActive(false);
		yesButton.gameObject.SetActive(true);
		noButton.gameObject.SetActive(true);
	}
	public void closeDialog(){
		gameObject.SetActive(false);
		yesButton.onClick.RemoveAllListeners();
		noButton.onClick.RemoveAllListeners();
		fireButton.onClick.RemoveAllListeners();
		iceButton.onClick.RemoveAllListeners();
		thunderButton.onClick.RemoveAllListeners();
		leafButton.onClick.RemoveAllListeners();
	}
}
