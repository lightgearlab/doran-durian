﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TurnManager))]
public class TurnEditor : Editor {

	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        TurnManager turn = (TurnManager)target;
        if(GUILayout.Button("End Turn"))
        {
            turn.endPlayerTurnNSetNextPlayerTurn();
        }

        if(GUILayout.Button("Throw Dice"))
        {
            turn.throwDice();
        }
        if(GUILayout.Button("Delete PlayerPref"))
        {
            turn.deletePlayerPref();
        }
    }
}
