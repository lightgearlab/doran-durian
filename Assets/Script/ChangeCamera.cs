﻿using UnityEngine;
using System.Collections.Generic;

public class ChangeCamera : MonoBehaviour {

	public List<Transform> cameraPos;
    public List<Transform> colliderPos;
	public int pos; 
	void OnTriggerEnter(Collider other) {
        
        //trigger when player enters
            if(other.tag == "Player"){
                Debug.Log("Changed camera to ->" + cameraPos[pos].position.ToString());
                GameObject.FindGameObjectWithTag("MainCamera").transform.localPosition = cameraPos[pos].position;
                GameObject.FindGameObjectWithTag("MainCamera").transform.rotation = cameraPos[pos].rotation;
            }
            
    }
    public void changeColliderPos(int p){
        pos = p;
        transform.position = colliderPos[pos].position;
    }
}
