﻿using UnityEngine;
using System.Collections;

public class AddDurian : MonoBehaviour {
    //public ParticleSystem hitParticle;
    public AudioClip hitClip;
	public bool respawn;
	
    // Use this for initialization
    void Start () {
        //hitParticle = GetComponent<ParticleSystem>();
    }

    void OnTriggerEnter(Collider other) {
        if(other.tag == "Player")
        {
            //remove object view
            MeshRenderer renderer = GetComponent<MeshRenderer>();
            renderer.enabled = false;
            
            //add particle
            //hitParticle.Play();

            //add sound
            AudioSource.PlayClipAtPoint(hitClip, gameObject.transform.position);
            
            
            //add score, will alter algorithm  based on level
            DoranPlayer player = other.GetComponent<DoranPlayer>();
            player.addDurian(2);
            
            SphereCollider sphereCollider = GetComponent<SphereCollider> ();
			sphereCollider.enabled=false;
            triggerDurian();

            StartCoroutine(EndTurn(other));
            
            Destroy(gameObject, 4);
            
        }
        
    }
    IEnumerator EndTurn(Collider other) {
        yield return new WaitForSeconds(1);
        other.GetComponent<DoranState>().endMyTurn();
    }
    public void triggerDurian(){
        
        GameObject durian = Instantiate(Resources.Load<GameObject>("Prefabs/Durian")) as GameObject;
        durian.transform.SetParent(GameObject.FindGameObjectWithTag("Durian").transform,false);
        Animator anim = durian.GetComponent<Animator>();
        float random = Random.Range(0f,3f);
        anim.SetFloat("Blend",random);
        anim.SetTrigger("durian");
        
        Destroy(durian,3f);
    }
}
