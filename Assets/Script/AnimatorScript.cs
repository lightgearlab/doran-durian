﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimatorScript : MonoBehaviour {

	BattleSceneManager battleSceneManager;
	// Use this for initialization
	void Start () {
		battleSceneManager = GameObject.FindGameObjectWithTag("BattleManager").GetComponent<BattleSceneManager>();
	}
	public void endChoice(){
		battleSceneManager.hideChoices();
	}
	// Update is called once per frame
	void Update () {
		
	}
}
