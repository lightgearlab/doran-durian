﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;
public class TurnManager : MonoBehaviour {
	//manage turn of players or monsters in main scene
	GameObject mainCanvas,upgradePanel;
	public string currentPlayerTurn;
	public Transform startPoint;
	public GameObject playerToSpawn;
	public List<GameObject> playerList;
	public List<GameObject> farmedPoints = new List<GameObject>();
	PlayerCamera playerCamera;
	public GameObject dice;
	public GameObject howManyPlayers;
	public int diceNumber;  
	static float DELAY_END_TURN = 1f;
	public static int turnCounter;
	int totalTurnCounter,tempCount,seasonCounter;
	public Text turn_text;
	public Slider season_slider;

	//temp
	public GameObject tree;

	// Use this for initialization
	void Awake () {
		playerCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<PlayerCamera>();
		mainCanvas = GameObject.FindGameObjectWithTag("MainCanvas");
        upgradePanel = GameObject.FindGameObjectWithTag("UpgradePanel");
		playerList = new List<GameObject>();
		//showHowManyPlayer();
		//will setup number of players prior from this screen
		//save number of player in shared pref
		//spawn player according to shared pref
		if(!PlayerPrefs.HasKey("amount")){
			//Debug.Log("Start");
			setupPlayers(2);
		}
		else{
			//start turn or continue from battle
			
			//Debug.Log("Continue");
			//get players
			getAndInstantiatePlayerList();

			if(PlayerPrefs.HasKey("battlePlayer")){
				//show player from the battle and end turn
				executePlayerAfterBattleAnimation();

				PlayerPrefs.DeleteKey("battlePlayer");

				StartCoroutine(DelayEndTurn());

			}else{
				//Debug.Log("Continue Turn");
				continueTurn();
			}
			
		}
		//temp
		turnCounter = 1;
		seasonCounter = 1;

	}
	void executePlayerAfterBattleAnimation(){
		string player_name = PlayerPrefs.GetString("battlePlayer");
		for(int i=0;i<playerList.Count;i++){
			if(playerList[i].name == player_name){
				string player_result = PlayerPrefs.GetString("mainPlayerResult");
				Debug.Log("From Battle" + player_name + " "+player_result);
				//player win
				if(player_result == "Win"){
					
					playerList[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("isSkill");
					//check which panel currently
					//control location
					//GameObject spot = playerList[i].GetComponent<DoranPlayer>().currentSpot;
					//save spot at players location
					GameObject spot = playerList[i];
					GameObject.Instantiate(tree,spot.transform.position,spot.transform.rotation);
					//playerList[i].GetComponent<DoranPlayer>().currentSpot
					playerList[i].GetComponent<DoranPlayer>().addDurian(2);
					//save player
					

					
				}else{
					playerList[i].transform.GetChild(0).GetComponent<Animator>().SetTrigger("IsAttacking");
					playerList[i].GetComponent<DoranPlayer>().minusDurian(2);
					//check which panel currently
					//if at player panel, add money to panel

				}
			}
		}
	}
	void getAndInstantiatePlayerList(){
		int num = PlayerPrefs.GetInt("amount");
		char[] d = {';'};
		string[] turns = PlayerPrefs.GetString("playerTurn").Split(d);
		//Debug.Log(PlayerPrefs.GetString("playerTurn"));
		for(int j=0;j<turns.Length;j++){
			string name = turns[j];
			string[] pos = PlayerPrefs.GetString(name).Split(d);
			GameObject obj = Instantiate(playerToSpawn,new Vector3(float.Parse(pos[0]),float.Parse(pos[1]),float.Parse(pos[2])),startPoint.rotation);
			obj.name = name;
			playerList.Add(obj);
		}
		
	}
	public void deletePlayerPref(){
		PlayerPrefs.DeleteAll();
	}
	void showHowManyPlayer(){
		howManyPlayers.SetActive(true);
	}
 	void setupPlayers(int numOfPlayers){
		//howManyPlayers.SetActive(false);
		for(int i=1;i<=numOfPlayers;i++){
			GameObject obj = Instantiate(playerToSpawn,new Vector3(startPoint.position.x+i,startPoint.position.y,startPoint.position.z),startPoint.rotation);
			obj.name = "Player "+(i);
			playerList.Add(obj);
		}
		turnCounter = 1;
		seasonCounter = 1;
		totalTurnCounter = 0;
		tempCount = 0;
		updateSeasonText();

		SaveInPlayerPref.saveCurrentPlayerLocation(playerList);
		setupFirstTurn();
	}
	
	public void setWhichPlayerBattling(){
		PlayerPrefs.SetString("battlePlayer",playerList[0].name);
	}
	void continueTurn(){
		//Debug.Log("setup turn 1 time");
		string player_name = PlayerPrefs.GetString("currentPlayer");
		for(int i=0;i<playerList.Count;i++){
			if(playerList[i].name == player_name){
				setPlayerTurn(playerList[i]);
			}
		}
	}
	void setupFirstTurn(){
		//setup based on random number
		for (int i = 0; i < playerList.Count; i++) {
			GameObject temp = playerList[i];
			int randomIndex = Random.Range(i, playerList.Count);
			playerList[i] = playerList[randomIndex];
			playerList[randomIndex] = temp;
		}
		setPlayerTurn(playerList[0]);
	}
	public void endPlayerTurnNSetNextPlayerTurn(){
		tempCount++;
		totalTurnCounter++;
		if(tempCount >= playerList.Count){
			turnCounter++;
			tempCount = 0;
			seasonCounter++;
			if(seasonCounter > 12)
				seasonCounter = 1;
			//turn ends
			foreach(GameObject p in farmedPoints){
				p.GetComponent<StopPoint>().minusTimeToHarvest(p);
			}
		}
		
		updateSeasonText();
		playerList[0].GetComponent<DoranState>().changeState(DoranState.State.START);
		GameObject temp = playerList[0];
		playerList.RemoveAt(0);
		playerList.Add(temp);

		setPlayerTurn(playerList[0]);
	}
	void updateSeasonText(){
		turn_text.text = "Week:"+turnCounter;
		season_slider.value = seasonCounter;
	}
	
	void setPlayerTurn(GameObject player){
		currentPlayerTurn = player.name;
		player.GetComponent<DoranState>().updatePlayerText();
		player.GetComponent<DoranPlayer>().updateDurianText();
		mainCanvas.GetComponent<CanvasScript>().updateElemStatus(player.GetComponent<DoranPlayer>());
		mainCanvas.GetComponent<CanvasScript>().updateElemText(player.GetComponent<DoranPlayer>().player);
		mainCanvas.GetComponent<CanvasScript>().updateBuahText(player.GetComponent<DoranPlayer>().player);
		player.GetComponent<DoranState>().changeStateAndExecuteTurn(DoranState.State.DICE);
		playerCamera.setTarget(player);
		dice.GetComponent<DiceScript>().showDice();
		
		SaveInPlayerPref.saveCurrentPlayer(player);
		SaveInPlayerPref.saveCurrentPlayerLocation(playerList);

	}
	 IEnumerator DelayEndTurn() {
		yield return new WaitForSeconds(DELAY_END_TURN);
		endPlayerTurnNSetNextPlayerTurn();
    }
	//not using
	public void throwDice(){
		int randomIndex = Random.Range(0, 6);
		//diceNumber = randomIndex+1;
		//playerList[0].GetComponent<DoranState>().moveCount = diceNumber;
		//show player Move
        playerList[0].GetComponent<DoranState>().changeStateAndExecuteTurn(DoranState.State.DICE_MOVE);
	}
	public void createSeed(){
		//create seed randomly on space
	}
}
